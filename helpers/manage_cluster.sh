#!/bin/bash

# we assume that CLUSTER[0] is the master.
CLUSTER=( "146.193.69.131" "146.193.69.132" "146.193.69.133" )
USER="eunomia"

function exec_cmd(){
    local node_ip="$1"
    local cmd="$2"
    local GREEN="\033[0;92m"
    local NORMAL="\033[0;27m"
    printf "***************************************************************************\n"
    printf "%s - executing: $GREEN %s $NORMAL\n" "$node_ip" "$cmd"
    ssh -t eunomia@$node_ip "PATH=$HOME/bin:$PATH;cd integrated-eunomia;$cmd"
    printf "\n"
}

function exec_all(){
    local cmd="$1"
    for node in "${CLUSTER[@]}"
    do
        exec_cmd "$node" "$cmd"
    done
}

CMD="$1"
exec_all "$CMD"

