# Storage&Security&Privacy Server

This component exposes the P2P layer of EUNOMINA and also provides additional services.

# Stable Release Information


*  Stable Tag: 
*  Most Recent development tag: **dev-20210204r1**

## Currently the following services are supported:


*  Authentication Service - Use this to authenticate users and components.
*  Storage Service - Use this to store arbitrary objects in the P2P storage.
*  Log Service - Use this to log misc messages into the node local log (this information is not sent to the P2P network)
*  Node Information Service - Use this to retrieve information about this node in particular.
*  Tracking Service - Use this to log important user actions that happen in within the network.


## The API can be accessed at

http://127.0.0.1:5000/api/swagger-ui.html


## Build instructions

To build this component you must install:

*  openjdk version "11.x.x"
*  Maven

Then proceed with in the root of the project (where pom.xml is located):

```
mvn clean package -DskitTests=true
```

## Generate Docker Runnable Image
In the root of the project after build (see previous section) execute:

```
docker build . -t storage-server
```

Optionally you can tag it with a given release, just don't forget to change the https://git-ext.inov.pt/cyber/eunomia/integrated-eunomia/-/blob/master/integrated-docker-compose-template.yml 
in order to include the new generated image for the storage-server container, and relaunch the node.

## Example docker compose:

```
    version: '3.7'
    
    storage-server:
        container_name: storage-server
        image: docker.eunomia.inov.pt/eunomia_storage-server:20201008r1stable
        depends_on:
            - orbitdb
        environment:
            - ORBITDB_URL=http://orbitdb:3000
            - ORBITDB_AUTH_DATABASE_CID=zdpuAt3PoHPNua4x6BJEDFPP7YSJvihADPTjDxkDRiUn5J5C4%2Fauth
            - ORBITDB_MODEL_DATABASE_CID=zdpuAnPwqKYn7gikwj4w2hDXKnJHFNJKkzwNLyL1qsrg9gZ8j%2Fmodel
            - ORBITDB_TRACK_DATABASE_CID=zdpuArb4MhRPsGN87JxLLwVwkodjzueA61Zd23xfHePHg5Ewk%2Ftrack
            - ORBITDB_DISCOVERY_DATABASE_CID=zdpuAuAdrf56Zg1RpGkE9mtGAE3mQn5rGKy6C8ekf2ynoYAmD%2Fdiscovery
            - ORBITDB_LEDGERCACHE_DATABASE_CID=zdpuB2pde5S9LRWG9wM2MsSWCVB4Wo27S2QQufEmKN4dnHhf3%2Fledgercache
            - ORBITDB_SN_MASTODON=/api/v1/accounts/verify_credentials
            - NODE_ID=NODE_UUID_PLACEHOLDER # replaced on setup
            - NODE_VOTING_SERVER_ID=VOTING_UUID_PLACEHOLDER # replaced on setup
            - NODE_DESCRIPTION=This is a node of the EUNOMIA Network.
            - LEDGER_URL=http://blockchain_api:9091/api/entries
            - NODE_URL=http://HOST_IP_PLACEHOLDER:5000 # replaced on setup
            - SN_FORCE_MASTODON_PROVIDER=DOMAIN_PLACEHOLDER # should be replaced on setup

```
