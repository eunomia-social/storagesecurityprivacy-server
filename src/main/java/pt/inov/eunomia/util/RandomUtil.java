/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.util;

import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.UUID;

/**
 * Several random generators for strings, integers, UUIDs, etc.
 * 
 * @author lmss
 */
public class RandomUtil {
    private static Logger logger = LoggerFactory.getLogger(RandomUtil.class);
    
    /**
     * Generates a random int between 0 < rnd < n
     * @param n The maximum bound
     * @return A random int.
     */
    public static int generateRandomInt(int n){
        return new Random().nextInt(n);
    }
    
    /**
     * Generates a random string with alphanumeric characters.
     * @param size The size of the string to be generated.
     * @return The random string.
     */
    public static String generateAlphanumeric(int size) {

        logger.info("generateAlphanumeric()");

        Random rand = new Random();
        String id = "";
        while (id.length() < size) {
            switch (rand.nextInt(4 - 1) + 1) {
                case 1:
                    int randNum = rand.nextInt((57 - 48) + 1) + 48;
                    id = id + ((char) randNum);
                    break;
                case 2:
                    int randUpperCaseLetter = rand.nextInt(90 - 65) + 65;
                    id = id + ((char) randUpperCaseLetter);
                    break;
                case 3:
                    int randLowerCaseLetter = rand.nextInt(122 - 97) + 97;
                    id = id + ((char) randLowerCaseLetter);
                    break;
            }
        }

        logger.info("generateAlphanumeric() returning alphanumeric generated");
        return id;
    }
    
    /**
     * Generate random UUID.
     * @return A string containing a UUID string.
     */
    public String generateUUID(){

        logger.info("generateUUID()");

        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();

        logger.info("generateUUID() returning UUID generated");

        return randomUUIDString;
    } 
}
