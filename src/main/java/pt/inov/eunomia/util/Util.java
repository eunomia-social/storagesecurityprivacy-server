/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.log4j.Logger;
import org.msgpack.jackson.dataformat.MessagePackFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * This class contains some common helper methods. Please note that if a given
 * set of functions placed here become too important or too specific please
 * reconsider refactoring them into a new class.
 *
 * @author lmss
 */
public class Util {

    private static final Logger logger = Logger.getLogger(Util.class);

    /**
     * Creates a byte array containing the signature of the provided object. The
     * hash algorithm is the following: 1- Creates a byte array containing the
     * object serialization using the message pack format. 2- Then it applies
     * SHA-256 to the array.
     *
     * @param obj The provided object, please note that the provided object must
     * be annotated with Message annotation.
     *
     * @return A byte array containing the SHA-256 signature of the object
     * provided or null in case of internal error.
     */
    public static byte[] toHash(Object obj) {

        logger.info("toHash()");

        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            ObjectMapper mapper = new ObjectMapper(new MessagePackFactory());
            byte[] serializedMsg = mapper.writeValueAsBytes(obj);

            logger.info("toHash() returning hashed object");

            return messageDigest.digest(serializedMsg);
        } catch (JsonProcessingException | NoSuchAlgorithmException e) {
            logger.error("toHash() Error serializing object.", e);
            return null;
        }
    }

    /**
     * Converts a given byte array to a string representation of hexadecimal
     * numbers.
     *
     * @param hashInBytes The given byte array.
     * @return A string containing the representation or null in case of
     * internal error.
     */
    public static String bytesToHex(byte[] hashInBytes) {

        logger.info("bytesToHex()");

        if (hashInBytes == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) {
            sb.append(String.format("%02x", b));
        }

        logger.info("bytesToHex()");

        return sb.toString();
    }

    /**
     * Instantiates a RestTemplate initialized with a given connection/read timeout.
     * @param timeoutMillis The timeout in milliseconds.
     * @return A newly created RestTemplate.
     * @see org.springframework.web.client.RestTemplate
     */
    public static RestTemplate getRestTemplate(int timeoutMillis) {
        RestTemplate restTemplate = new RestTemplate();
        ((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory()).setConnectTimeout(timeoutMillis);
        ((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory()).setReadTimeout(timeoutMillis);
        return restTemplate;
    }
}
