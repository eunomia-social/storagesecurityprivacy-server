/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.db.orbitdb;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.inov.eunomia.util.RandomUtil;

/**
 * Orbit Database Client for Authentication.
 */
public class AuthOrbitDbClient extends OrbitDbClient {

    private static final Logger log = LoggerFactory.getLogger(AuthOrbitDbClient.class);

    public static final int TOKEN_SIZE = 50;

    private final ObjectMapper objectMapper;
    

    public AuthOrbitDbClient(String url, String globalAddress, int timeoutMillis) {
        super(url, globalAddress,timeoutMillis);
        log.info("Instantiated with URL:"+url+" globalAddress:"+globalAddress+" timeoutMillis:"+timeoutMillis+" ms.");
        objectMapper = new ObjectMapper();
    }

    public String generateToken() {

        log.info("generateToken()");

        String token = "";
        while (!super.checkItemHasTheSizeSpecified(token, TOKEN_SIZE)) {
            token = RandomUtil.generateAlphanumeric(TOKEN_SIZE);
        }

        log.info("generateToken() returning the EUNOMIA Token generated");

        return token;
    }
}
