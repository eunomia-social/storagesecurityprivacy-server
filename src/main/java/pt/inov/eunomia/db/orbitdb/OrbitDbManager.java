/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.db.orbitdb;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pt.inov.eunomia.conf.OrbitDbConfig;
import pt.inov.eunomia.util.Util;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Component
public class OrbitDbManager {

    private static final Logger log = LoggerFactory.getLogger(OrbitDbManager.class);

    @Autowired
    private OrbitDbConfig orbitDbConfig;

    public void initialize() {

        log.info("initialize()");

        HashMap<String, String> databases = new HashMap<>();
        databases.put("model", "id");
        databases.put("auth", "token");
        databases.put("track", "id");
        databases.put("discovery", "id");
        databases.put("ledgercache", "id");

        for (Map.Entry<String, String> database : databases.entrySet()) {
            if (!checkDatabaseExists(database.getKey())) {
                createDatabase(database.getKey(), database.getValue());
            } else {
                log.info("Database " + database.getKey() + " already exists");
            }
        }

        log.info("initialize() finished");
    }

    private boolean createDatabase(String databaseName, String indexBy) {

        log.info("createDatabase()");

        boolean result = false;

        ObjectMapper objectMapper = new ObjectMapper();

        RestTemplate restTemplate = Util.getRestTemplate(this.orbitDbConfig.getTimeoutMillis());
        String url = this.orbitDbConfig.getUrl() + "/db/" + databaseName;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String payload = "{\"create\":true,\"type\":\"docstore\",\"accessController\":{\"write\":[\"*\"]}, \"indexBy\":\"" + indexBy + "\"}";

        try {
            ObjectNode object = objectMapper.readValue(payload, ObjectNode.class);
            HttpEntity<ObjectNode> request = new HttpEntity<ObjectNode>(object, headers);
            log.info("createDateBase() Creating database by posting to '"+url + "' with schema:'"+payload+"'");
            URI uri = new URI(url);
            restTemplate.postForObject(uri, request, String.class);
            result = true;
        } catch (Exception e) {
            log.error("Error creating " + databaseName + ": ", e);
        }

        log.info("createDatabase() database " + databaseName + " created");

        return result;
    }

    private boolean checkDatabaseExists(String databaseName) {

        log.info("checkDatabaseExists()");

        boolean found = false;
        RestTemplate restTemplate = Util.getRestTemplate(this.orbitDbConfig.getTimeoutMillis());
        ArrayNode databases = null;
        try {
            URI uri = new URI(this.orbitDbConfig.getUrl() + "/dbs");
            databases = restTemplate.getForObject(uri, ArrayNode.class);
        } catch (Exception e) {
            log.error("databaseExists() Error: ", e);
        }

        if (databases.size() == 0) {
            log.info("checkDatabaseExists() databases is empty");
            return found;
        }

        for (JsonNode database : databases) {
            if (databaseName.equals(database.get("dbname").asText())) {
                found = true;
            }
        }

        log.info("checkDatabaseExists() returning " + found);

        return found;
    }
}
