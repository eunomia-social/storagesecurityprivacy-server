/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.db.orbitdb;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import pt.inov.eunomia.util.Util;

public class OrbitDbClient {

    private static final Logger log = LoggerFactory.getLogger(OrbitDbClient.class);

    private String databaseUrl;

    private String databaseName;

    private int timeoutMillis;

    public OrbitDbClient(String url, String globalAddress, int timeoutMillis) {
        this.databaseUrl = url + "/db/" + globalAddress;
        //try to extract database name:
        try {
            this.databaseName = this.databaseUrl.split("%2F")[1];
        } catch (Exception e) {
            this.databaseName = globalAddress;

        }
        this.timeoutMillis = timeoutMillis;
        log.warn("OrbitDbClient() using database name=" + databaseName);
    }

    public ObjectNode createItem(ObjectNode item) {

        log.info("createItem()");

        if (!this.databaseExists()) {
            log.error("createItem() Error: Database " + this.databaseName + " doesn't exists");
            return null;
        }

        try {
            RestTemplate restTemplate = Util.getRestTemplate(timeoutMillis);
            String url = this.databaseUrl + "/put";
            URI uri = new URI(url);

            restTemplate.postForObject(uri, item, String.class);
        } catch (Exception e) {
            log.error("createItem() Error: ", e);
        }

        log.info("createItem() returning Item created");

        return item;
    }

    public ObjectNode getItem(String itemId) {

        ObjectNode ret = null;

        log.info("getItem() with id: " + itemId);

        if (!this.databaseExists()) {
            log.error("getItem() Error: Database " + this.databaseName + " doesn't exists");
            return null;
        }

        RestTemplate restTemplate = Util.getRestTemplate(timeoutMillis);
        String url = this.databaseUrl + "/" + itemId;
        String item = null;
        try {
            URI uri = new URI(url);
            item = restTemplate.getForObject(uri, String.class);
            ObjectMapper objectMapper = new ObjectMapper();
            ArrayNode items = objectMapper.readValue(item, ArrayNode.class);
            ret = (ObjectNode) items.get(0);
        } catch (Exception e) {
            log.error("getItem() Error: ", e);
        }

        log.info("getItem() returning the Item fetched");

        return ret;
    }

    public void deleteItem(String itemId) {

        log.info("deleteItem()");

        if (!this.databaseExists()) {
            log.error("deleteItem() Error: Database " + this.databaseName + " doesn't exists");
            return;
        }

        try {
            RestTemplate restTemplate = Util.getRestTemplate(timeoutMillis);
            String url = this.databaseUrl + "/" + itemId;
            URI uri = new URI(url);
            restTemplate.delete(uri);
        } catch (Exception e) {
            log.error("deleteItem() Error: ", e);
        }

        log.info("deleteItem() item deleted");
    }

    public ArrayNode getItemsByProperty(String[] properties, String[] comps, String[] values) {
        log.info("getItemsByProperty()");
        
        if (!this.databaseExists()) {
            log.error("getItemsByProperty() Error: Database " + this.databaseName + " doesn't exists");
            return null;
        }

        RestTemplate restTemplate = Util.getRestTemplate(timeoutMillis);
        String url = this.databaseUrl + "/query";

        ObjectMapper objectMapper = new ObjectMapper();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        ObjectNode object = objectMapper.createObjectNode();
        object.put("props", objectMapper.valueToTree(properties));
        object.put("comps", objectMapper.valueToTree(comps));
        object.put("values", objectMapper.valueToTree(values));
        try{
            log.info("getItemsByProperty() Posting filter:"+new ObjectMapper().writeValueAsString(object));
        }catch(JsonProcessingException e){
            log.error("getItemsByProperty() Error parsing request filter to show in the log.",e);
        }

        HttpEntity<ObjectNode> request = new HttpEntity<ObjectNode>(object, headers);

        ArrayNode items = null;

        try {
            URI uri = new URI(url);
            items = restTemplate.postForObject(uri, request, ArrayNode.class);
        } catch (Exception e) {
            log.error("getItemsByProperty() Error: ", e);
        }

        log.debug("getItemsByProperty() returned Items array");
        return items;
    }

    public boolean checkItemHasTheSizeSpecified(String id, int size) {

        log.info("checkItemHasTheSizeSpecified(): " + size);

        return !this.checkItemExists(id) && id.length() == size;
    }

    private boolean checkItemExists(String id) {

        log.info("checkItemExists()");

        if (id.isEmpty()) {
            return false;
        }

        log.info("checkItemExists() returning");

        return this.getItem(id) != null;
    }

    private boolean databaseExists() {
        RestTemplate restTemplate = Util.getRestTemplate(timeoutMillis);
        String url = this.databaseUrl;
        JsonNode database = null;
        try {
            URI uri = new URI(url);
            database = restTemplate.getForObject(uri, JsonNode.class);
        } catch (Exception e) {
            log.error("databaseExists() Error: ", e);
        }

        if (database == null) {
            log.error("databaseExists() DataBase:"+url+" doesn't exist.");
            return false;
        }

        if (!database.has("dbname")) {
            log.error("databaseExists() DataBase:"+url+" doesn't exist.");
            return false;
        }
        return this.databaseName.equals(database.get("dbname").asText());
    }

    /**
     * Get the underlying connection/read timeout in milliseconds.
     *
     * @return
     */
    public int getTimeoutMillis() {
        return timeoutMillis;
    }

    /**
     * Set the underlying connection/read timeout in milliseconds.
     *
     * @param timeoutMillis the timeoutMillis to set
     */
    public void setTimeoutMillis(int timeoutMillis) {
        this.timeoutMillis = timeoutMillis;
    }
}
