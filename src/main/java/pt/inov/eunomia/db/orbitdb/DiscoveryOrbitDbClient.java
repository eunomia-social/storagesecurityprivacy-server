/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.db.orbitdb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.inov.eunomia.util.RandomUtil;

public class DiscoveryOrbitDbClient extends OrbitDbClient {

    private static final Logger log = LoggerFactory.getLogger(DiscoveryOrbitDbClient.class);

    private static final int ID_SIZE = 20;

    public DiscoveryOrbitDbClient(String url, String globalAddress, int timeoutMillis) {
        super(url, globalAddress, timeoutMillis);
        log.info("Instantiated with URL:"+url+" globalAddress:"+globalAddress+" timeoutMillis:"+timeoutMillis+" ms.");
    }

    public String generateId() {

        log.info("generateId()");

        String id = "";
        while (!super.checkItemHasTheSizeSpecified(id, ID_SIZE)) {
            id = RandomUtil.generateAlphanumeric(ID_SIZE);
        }

        log.info("generateId() returning the id generated");

        return id;
    }
}
