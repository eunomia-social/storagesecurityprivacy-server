/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.db.ledger;

import pt.inov.eunomia.model.ledger.LedgerEntryQuery;
import pt.inov.eunomia.model.ledger.LedgerEntry;
import pt.inov.eunomia.model.ledger.LedgerEntriesIndexed;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import pt.inov.eunomia.util.Util;

/**
 * Ledger invocation client. It contains the stubs to call the Ledger API
 * methods. Only uses LedgerEntry as model.
 */
public class LedgerDbClient {

    private static final Logger log = LoggerFactory.getLogger(LedgerDbClient.class);
    private final String baseUrl;
    private int timeoutMillis;

    public LedgerDbClient(String baseUrl, int timeoutMillis) {
        this.baseUrl = baseUrl;
        this.timeoutMillis = timeoutMillis;
        log.info("Instantiated with URL:" + baseUrl + " timeoutMillis:" + timeoutMillis + " ms.");
    }

    public LedgerEntry getEntry(int id, String type) {
        LedgerEntry ledgerEntry = null;
        log.info("getEntry() with id " + id + " and type " + type);
        try {
            RestTemplate restTemplate = Util.getRestTemplate(timeoutMillis);
            String typeUrlEncoded = URLEncoder.encode(type, StandardCharsets.UTF_8.toString());
            String url = this.baseUrl + "entries/" + typeUrlEncoded + "/" + id + "/";
            URI uri = new URI(url);
            log.info("getEntry() Executing GET:" + url);
            String response = restTemplate.getForObject(uri, String.class);
            if (!"\"\"".equals(response)) {
                log.debug("getEntry() With id:" + id + " type:" + type + " returned: '" + response + "'");
                ObjectMapper objectMapper = new ObjectMapper();
                ledgerEntry = objectMapper.readValue(response, LedgerEntry.class);
            }
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                log.info("getEntry() with id " + id + " and type " + type + " not found:" + e);
            } else {
                log.error("getEntry() Error: ", e);
            }
            ledgerEntry = null;
        } catch (Exception e) {
            log.error("getEntry() Error: ", e);
            ledgerEntry = null;
        }
        log.info("getEntry() returning the Ledger Entry fetched:" + ledgerEntry);
        return ledgerEntry;
    }

    // TODO: implement getEntryHistory()
    public void getEntryHistory(int id, String type) {
        log.error("getEntryHistory() This method is not implemented.");
        try {
            RestTemplate restTemplate = Util.getRestTemplate(timeoutMillis);
            String typeUrlEncoded = URLEncoder.encode(type, StandardCharsets.UTF_8.toString());
            String url = this.baseUrl + "entries/" + type + "/" + id + "/history";
        } catch (Exception e) {
            log.error("getEntryHistory() ", e);
        }
        // URI uri = new URI(url);
    }

    public boolean createEntry(LedgerEntry ledgerEntry) {
        log.info("createEntry() with id: " + ledgerEntry.getId());
        boolean ret = false;
        RestTemplate restTemplate = Util.getRestTemplate(timeoutMillis);
        String response = null;
        try {
            URI uri = new URI(this.baseUrl+"entries/");
            response = restTemplate.postForObject(uri, ledgerEntry, String.class);
            if ("true".equals(response)) {
                ret = true;
            }
        } catch (Exception e) {
            ret = false;
            log.error("createEntry() Error: ", e);
        }

        log.info("createEntry() returning the Ledger Entry created");

        return ret;
    }

    public LedgerEntriesIndexed getEntries(ArrayList<LedgerEntryQuery> ledgerQuery) {
        LedgerEntriesIndexed entries = new LedgerEntriesIndexed();
        log.info("getEntries() called for: " + ledgerQuery);
        if (ledgerQuery == null) {
            log.info("getEntries() null ledger query received, so returning null");
            return null;
        }
        try {
            RestTemplate restTemplate = Util.getRestTemplate(timeoutMillis);
            String url = this.baseUrl + "entries_batch" + "/";
            log.info("getEntries() Executing POST:" + url);
            HttpHeaders headers = new HttpHeaders();
            HttpEntity<List<LedgerEntryQuery>> request = new HttpEntity<List<LedgerEntryQuery>>(ledgerQuery, headers);
            ResponseEntity<List<LedgerEntry>> response = restTemplate.exchange(url, HttpMethod.POST, request, new ParameterizedTypeReference<List<LedgerEntry>>() {
            });
            List<LedgerEntry> ledgerEntries = response.getBody();
            if (ledgerEntries == null) { //they send null instead []
                ledgerEntries = new ArrayList<>();
            }
            log.info("getEntries() Getted " + ledgerEntries.size() + " entries.");
            for (LedgerEntry ledgerEntry : ledgerEntries) {
                entries.indexByTypeById(ledgerEntry);
            }
        } catch (HttpClientErrorException e) {
            entries = null;
        } catch (Exception e) {
            log.error("getEntry() Error: ", e);
            entries = null;
        }
        log.info("getEntry() returning the Ledger Entry fetched:" + entries);
        return entries;
    }

    /**
     * @return the timeoutMillis
     */
    public int getTimeoutMillis() {
        return timeoutMillis;
    }

    /**
     * @param timeoutMillis the timeoutMillis to set
     */
    public void setTimeoutMillis(int timeoutMillis) {
        this.timeoutMillis = timeoutMillis;
    }
}
