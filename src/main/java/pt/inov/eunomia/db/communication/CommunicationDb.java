/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.db.communication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CommunicationDb {

    private static final Logger log = LoggerFactory.getLogger(CommunicationDb.class);

    protected CommunicationDb() {
    }

    protected Connection getConnection() {

        log.debug("getConnection() init");

        Connection connection = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:communicationDb.sqlite");
        } catch (SQLException e) {
            log.error("Error: ", e);
        }

        log.debug("getConnection() finished");

        return connection;
    }

    protected PreparedStatement newQuery(String queryString) {

        log.debug("newQuery() init");

        PreparedStatement ps = null;
        Connection connection = this.getConnection();

        if (connection != null) {
            try {
                ps = connection.prepareStatement(queryString);
            } catch (SQLException e) {
                log.error("Error: ", e);
            }
        }

        log.debug("newQuery() finished");

        return ps;
    }

    protected boolean endQuery(PreparedStatement ps) {

        log.debug("endQuery() init");

        boolean result = false;
        if (ps != null) {
            try {
                ps.getConnection().close();
                ps.close();
                result = true;
            } catch (SQLException e) {
                log.error("Error: ", e);
            }
        }

        log.debug("endQuery() finished");

        return result;
    }
}
