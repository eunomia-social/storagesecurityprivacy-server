/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.db.communication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pt.inov.eunomia.model.communication.Message;
import pt.inov.eunomia.model.communication.Receiver;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@Service
public class Inbox extends MessagePersistence {

    private static final Logger log = LoggerFactory.getLogger(Inbox.class);

    private static final String TABLE = "inbox";

    private static final String PIVOT = "inboxReceivers";

    private static final String QUERY_GET_PENDING_MESSAGES = "SELECT serviceId, callback as serviceCallback, name as serviceName, messageId, status, timestamp, content, sender, nodeId\n" +
            "FROM services\n" +
            "JOIN (\n" +
            "    SELECT inbox.id as messageId, inbox.sender, inbox.content, inbox.status, inbox.timestamp, inboxReceivers.nodeId, inboxReceivers.serviceId\n" +
            "    FROM inbox\n" +
            "    JOIN inboxReceivers\n" +
            "    ON inbox.id = inboxReceivers.messageId\n" +
            "    WHERE inbox.status = 'Pending'\n" +
            "    ) as messages\n" +
            "ON services.id = messages.serviceId;";

    private static final String QUERY_GET_SERVICE_INBOX = "SELECT DISTINCT serviceId, callback as serviceCallback, name as serviceName, messageId, status, timestamp, content, sender, nodeId\n" +
            "FROM services\n" +
            "JOIN (\n" +
            "    SELECT inbox.id as messageId, inbox.sender, inbox.content, inbox.status, inbox.timestamp, inboxReceivers.nodeId, inboxReceivers.serviceId\n" +
            "    FROM inbox\n" +
            "    JOIN inboxReceivers\n" +
            "    ON inbox.id = inboxReceivers.messageId\n" +
            "    WHERE inbox.status = 'Pending'\n" +
            "    ) as messages\n" +
            "ON services.id = messages.serviceId\n" +
            "WHERE serviceId = ";

    public void createInbox() {
        log.info("createInbox() init");
        this.createTables(TABLE, PIVOT);
        log.info("createInbox() finished");
    }

    public boolean saveMessage(Message message) {
        return this.createMessage(TABLE, PIVOT, message);
    }

    public void removeReceivers(Message message, ArrayList<Receiver> receivers) {
        this.removeReceivers(PIVOT, message, receivers);
    }

    public void setMessagesAsSent(ArrayList<Message> messages) {
        this.setMessagesAsSent(TABLE, messages);
    }

    public void clearSentMessages() {
        this.clearSentMessages(TABLE, PIVOT);
    }

    public void clearOldMessages(int temporalCriteria) {
        this.clearOldMessages(TABLE, PIVOT, temporalCriteria);
    }

    public ArrayList<Message> getServiceMessages(String serviceId) {
        log.debug("getServiceMessages() init");

        ArrayList<Message> messages = new ArrayList<>();

        PreparedStatement ps = null;

        try {
            ps = newQuery(QUERY_GET_SERVICE_INBOX + serviceId + ";");

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                Message message = new Message();
                message.setId(resultSet.getLong("messageId"));
                message.setSender(resultSet.getString("sender"));
                message.setContent(resultSet.getString("content"));
                message.setStatus(resultSet.getString("status"));
                message.setTimestamp(resultSet.getLong("timestamp"));
                message.getReceivers().add(new Receiver(
                        resultSet.getString("nodeId"),
                        resultSet.getString("serviceId")
                ));
                messages.add(message);
            }
        } catch (SQLException e) {
            log.error("Error: ", e);
        } finally {
            endQuery(ps);
        }

        log.debug("getServiceMessages() messages found: " + messages.size());

        return messages;
    }

    public Object[] getPendingMessagesWithServices() {
        log.debug("getPendingMessagesWithServices() init");

        ArrayList<Message> messages = new ArrayList<>();
        ArrayList<pt.inov.eunomia.model.communication.Service> services = new ArrayList<>();
        PreparedStatement ps = null;

        try {
            ps = newQuery(QUERY_GET_PENDING_MESSAGES);

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                boolean messageFound = false;
                // check message already exists
                for (Message message : messages) {
                    if (message.getId() == resultSet.getLong("messageId")) {
                        // add receiver
                        message.getReceivers().add(new Receiver(
                                resultSet.getString("nodeId"),
                                resultSet.getString("serviceId")
                        ));
                        messageFound = true;
                    }
                }

                if (! messageFound) {
                    Message message = new Message();
                    message.setId(resultSet.getLong("messageId"));
                    message.setSender(resultSet.getString("sender"));
                    message.setContent(resultSet.getString("content"));
                    message.setStatus(resultSet.getString("status"));
                    message.setTimestamp(resultSet.getLong("timestamp"));
                    message.getReceivers().add(new Receiver(
                            resultSet.getString("nodeId"),
                            resultSet.getString("serviceId")
                    ));
                    messages.add(message);
                }

                // check service already exists
                if (! this.checkServiceExists(resultSet.getString("serviceId"), services)) {
                    services.add(new pt.inov.eunomia.model.communication.Service(
                            resultSet.getString("serviceId"),
                            resultSet.getString("serviceName"),
                            resultSet.getString("serviceCallback")
                    ));
                }
            }

        } catch (SQLException e) {
            log.error("Error: ", e);
        } finally {
            endQuery(ps);
        }

        log.debug("getPendingMessagesWithServices() finished");

        return new Object[] {messages, services};
    }
}
