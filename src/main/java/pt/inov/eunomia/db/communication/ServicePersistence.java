/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.db.communication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.inov.eunomia.model.communication.Service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@org.springframework.stereotype.Service
public class ServicePersistence extends CommunicationDb {

    private static final Logger log = LoggerFactory.getLogger(ServicePersistence.class);

    public void createTable() {

        log.info("Creating services table");

        PreparedStatement ps = null;
        try {
            ps = newQuery("CREATE TABLE IF NOT EXISTS services (\n"
                    + "   id TEXT NOT NULL,\n"
                    + "   name TEXT NOT NULL,\n"
                    + "   callback TEXT NOT NULL\n"
                    + ");");
            ps.executeUpdate();
        } catch (SQLException e) {
            log.error("createTable() Error creating table.", e);
        } finally {
            endQuery(ps);
        }

        log.info("Services table created");
    }

    public ArrayList<Service> getAllServices() {
        ArrayList<Service> services = new ArrayList<>();
        PreparedStatement ps = null;

        try {
            ps = newQuery("SELECT * FROM services");
            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                Service service = new Service(
                        resultSet.getString("id"),
                        resultSet.getString("name"),
                        resultSet.getString("callback")
                );
                services.add(service);
            }

        } catch (SQLException e) {
            log.error("getAllServices()", e);
        } finally {
            endQuery(ps);
        }

        return services;
    }

    public Service createService(Service service) {
        PreparedStatement ps = null;

        try {
            ps = newQuery("INSERT INTO services(id, name, callback) VALUES(?, ?, ?)");
            ps.setString(1, service.getId());
            ps.setString(2, service.getName());
            ps.setString(3, service.getCallback());
            ps.executeUpdate();
        } catch (SQLException e) {
            log.error("createService()", e);
        } finally {
            endQuery(ps);
        }

        return service;
    }

    public Service getService(String id) {
        Service service = null;
        PreparedStatement ps = null;

        try {
            ps = newQuery("SELECT * FROM services WHERE id = ?");
            ps.setString(1, id);
            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                service = new Service();
                service.setId(resultSet.getString("id"));
                service.setName(resultSet.getString("name"));
                service.setCallback(resultSet.getString("callback"));
            }
        } catch (SQLException e) {
            log.error("getService()", e);
        } finally {
            endQuery(ps);
        }

        return service;
    }

    public void deleteService(String id) {
        PreparedStatement ps = null;

        try {
            ps = newQuery("DELETE FROM services WHERE id = ?");
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            log.error("deleteService() Error for id=" + id, e);
        } finally {
            endQuery(ps);
        }
    }
}
