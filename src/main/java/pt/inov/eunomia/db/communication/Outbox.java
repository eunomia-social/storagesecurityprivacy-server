/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.db.communication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pt.inov.eunomia.model.communication.Message;
import pt.inov.eunomia.model.communication.Receiver;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@Service
public class Outbox extends MessagePersistence {

    private static final Logger log = LoggerFactory.getLogger(Outbox.class);

    private static final String TABLE = "outbox";

    private static final String PIVOT = "outboxReceivers";

    private static final String QUERY_GET_PENDING_MESSAGES = "SELECT outbox.id, sender, content, status, nodeId, serviceId, timestamp\n" +
            "FROM outbox\n" +
            "JOIN outboxReceivers\n" +
            "ON outbox.id = outboxReceivers.messageId\n" +
            "WHERE status = 'Pending';";

    public void createOutbox() {
        this.createTables(TABLE, PIVOT);
    }

    public void removeReceivers(Message message, ArrayList<Receiver> receivers) {
        this.removeReceivers(PIVOT, message, receivers);
    }

    public void setMessagesAsSent(ArrayList<Message> messages) {
        this.setMessagesAsSent(TABLE, messages);
    }

    public void clearSentMessages() {
        this.clearSentMessages(TABLE, PIVOT);
    }

    public void clearOldMessages(int temporalCriteria) {
        this.clearOldMessages(TABLE, PIVOT, temporalCriteria);
    }

    public boolean saveMessage(Message message) {
        return this.createMessage(TABLE, PIVOT, message);
    }

    public ArrayList<Message> getPendingMessages() {
        log.info("getPendingMessages() init");
        ArrayList<Message> messages = new ArrayList<>();
        PreparedStatement ps = null;

        try {
            ps = newQuery(QUERY_GET_PENDING_MESSAGES);
            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                boolean messageFound = false;
                for (Message message : messages) {
                    if (message.getId() == resultSet.getLong("id")) {
                        message.getReceivers().add( new Receiver(
                                resultSet.getString("nodeId"),
                                resultSet.getString("serviceId")
                        ));
                        messageFound = true;
                    }
                }

                if (! messageFound) {
                    Message message = this.createMessage(resultSet);
                    messages.add(message);
                }
            }

        } catch (SQLException e) {
            log.error("Error: ", e);
        } finally {
            endQuery(ps);
        }

        log.info("getPendingMessages() finished");

        return messages;
    }

    public ArrayList<Message> getPendingMessagesWithOneReceiver() {
        log.debug("getPendingMessagesWithOneReceiver() init");
        ArrayList<Message> messages = new ArrayList<>();
        PreparedStatement ps = null;

        try {
            ps = newQuery(QUERY_GET_PENDING_MESSAGES);
            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                Message message = this.createMessage(resultSet);
                messages.add(message);
            }

        } catch (SQLException e) {
            log.error("Error: ", e);
        } finally {
            endQuery(ps);
        }

        log.debug("getPendingMessagesWithOneReceiver() finished");

        return messages;
    }

    private Message createMessage(ResultSet resultSet) {
        Message message = new Message();

        try {
            message.setId(resultSet.getLong("id"));
            message.setSender(resultSet.getString("sender"));
            message.setContent(resultSet.getString("content"));
            message.setStatus(resultSet.getString("status"));
            message.setTimestamp(resultSet.getLong("timestamp"));
            message.getReceivers().add(new Receiver(
                    resultSet.getString("nodeId"),
                    resultSet.getString("serviceId")
            ));
        } catch (SQLException e) {
            log.error("Error: ", e);
        }

        return message;
    }
}
