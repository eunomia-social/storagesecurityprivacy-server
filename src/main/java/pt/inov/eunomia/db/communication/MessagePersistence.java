/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.db.communication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.inov.eunomia.model.communication.Message;
import pt.inov.eunomia.model.communication.Receiver;
import pt.inov.eunomia.model.communication.Service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MessagePersistence extends CommunicationDb {

    private static final Logger log = LoggerFactory.getLogger(MessagePersistence.class);

    public void createTables(String table, String pivotName) {

        log.info("createTables() init");

        PreparedStatement ps = null;
        try {
            ps = newQuery("CREATE TABLE IF NOT EXISTS " + table + "(\n" +
                    "   id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                    "   sender TEXT NOT NULL,\n" +
                    "   content TEXT NOT NULL,\n" +
                    "   status TEXT NOT NULL,\n" +
                    "   timestamp LONG NOT NULL\n" +
                    ");");
            ps.executeUpdate();
        } catch (SQLException e){
            log.error("Error: ", e);
        } finally {
            endQuery(ps);
        }

        try {
            ps = newQuery("CREATE TABLE IF NOT EXISTS " + pivotName + "(\n" +
                    "   id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                    "   nodeId TEXT NOT NULL,\n" +
                    "   serviceId TEXT NOT NULL,\n" +
                    "   messageId TEXT NOT NULL\n" +
                    ");");
            ps.executeUpdate();
        } catch (SQLException e){
            log.error("Error: ", e);
        } finally {
            endQuery(ps);
        }

        log.info("createTables() finished");

    }

    public boolean createMessage(String table, String pivot, Message message) {

        log.debug("createMessage() init");

        boolean messageCreated = false;
        PreparedStatement ps = null;

        try {
            ps = newQuery("INSERT INTO " + table + "(sender, content, status, timestamp) VALUES(?, ?, ?, ?)");
            ps.setString(1, message.getSender());
            ps.setString(2, message.getContent());
            ps.setString(3, message.getStatus());
            ps.setLong(4, message.getTimestamp());
            ps.executeUpdate();
            ResultSet resultSet = ps.getGeneratedKeys();
            message.setId(resultSet.getLong(1));
            messageCreated = true;
        } catch (SQLException e) {
            log.error("Error: ", e);
        } finally {
            endQuery(ps);
        }

        ps = null;
        for (Receiver receiver: message.getReceivers()) {
            try {
                ps = newQuery("INSERT INTO " + pivot + "(nodeId, serviceId, messageId) VALUES(?, ?, ?)");
                ps.setString(1, receiver.getNodeId());
                ps.setString(2, receiver.getServiceId());
                ps.setString(3, String.valueOf(message.getId()));
                ps.executeUpdate();
            } catch (SQLException e) {
                log.error("Error: ", e);
            } finally {
                endQuery(ps);
            }
        }

        log.debug("createMessage() finished");

        return messageCreated;
    }

    public boolean checkServiceExists(String serviceId, ArrayList<Service> services) {
        log.debug("checkServiceExists() init");
        for (Service service : services) {
            if (service.getId().equals(serviceId)) {
                return true;
            }
        }
        log.debug("checkServiceExists() finished");
        return false;
    }

    public void setMessagesAsSent(String table, ArrayList<Message> messages) {
        log.debug("setMessagesAsSent() init: " + messages.size() + " messages");
        PreparedStatement ps = null;
        String query = "UPDATE " + table + " SET status = 'Sent' WHERE";
        StringBuilder stringBuilder = new StringBuilder(query);

        for (int i = 0; i < messages.size(); i++) {
            stringBuilder.append(" id = ").append(messages.get(i).getId());
            if (i == messages.size() - 1) {
                stringBuilder.append(";");
            } else {
                stringBuilder.append(" OR");
            }
        }

        try {
            log.debug("QUERY: " + stringBuilder.toString());
            ps = newQuery(stringBuilder.toString());
            int numberOfMessages = ps.executeUpdate();
            log.debug("setMessagesAsSent() number of messages set as sent: " + numberOfMessages);
        } catch (SQLException e) {
            log.error("Error: " + e);
        } finally {
            endQuery(ps);
        }

        log.debug("setMessagesAsSent() finished");
    }

    public void clearSentMessages(String table, String pivot) {
        log.debug("clearSentMessages() init");
        PreparedStatement ps = null;

        try {
            ps = newQuery("DELETE FROM " + pivot + "\n" +
                    "WHERE messageId in (\n" +
                    "    SELECT id\n" +
                    "    FROM "+ table + "\n" +
                    "    WHERE status = 'Sent'\n" +
                    ");");

            ps.executeUpdate();
        } catch (SQLException e) {
            log.error("Error: ", e);
        } finally {
            endQuery(ps);
        }

        try {
            ps = newQuery("DELETE FROM " + table + " WHERE status = 'Sent';");
            int messagesDeleted = ps.executeUpdate();
            log.debug("clearSentMessages() number of messages deleted: " + messagesDeleted);
        } catch (SQLException e) {
            log.error("Error: ", e);
        } finally {
            endQuery(ps);
        }

        log.debug("clearSentMessages() finished");
    }

    public void clearOldMessages(String table, String pivot, int temporalCriteria) {
        log.debug("clearOldMessages() init");
        PreparedStatement ps = null;

        long oldTimestamp = System.currentTimeMillis() - temporalCriteria;

        try {
            ps = newQuery("DELETE FROM " + pivot + "\n" +
                    "WHERE messageId IN (\n" +
                    "    SELECT id\n" +
                    "    FROM " + table + "\n" +
                    "    WHERE timestamp < " + oldTimestamp + "\n" +
                    ");");
            ps.executeUpdate();
        } catch(SQLException e) {
            log.error("Error: ", e);
        } finally {
            endQuery(ps);
        }

        try {
            ps = newQuery("DELETE FROM " + table + " WHERE timestamp < " + oldTimestamp);
            int deletedMessages = ps.executeUpdate();
            log.debug("clearOldMessages() old messages deleted: " + deletedMessages);
        } catch(SQLException e) {
            log.error("Error: ", e);
        } finally {
            endQuery(ps);
        }

        log.debug("clearOldMessages() finished");
    }

    public void removeReceivers(String pivot, Message message, ArrayList<Receiver> receivers) {
        log.debug("removeReceivers() init");
        PreparedStatement ps = null;
        String query = "DELETE FROM inboxReceivers\n" +
                "WHERE " + pivot + ".messageId = " + message.getId() + "\n" +
                "AND (";

        StringBuilder stringBuilder = new StringBuilder(query);

        for (int i = 0; i < receivers.size(); i++) {
            stringBuilder.append(pivot).append(".serviceId = ").append(receivers.get(i).getServiceId());
            if (i == receivers.size() - 1) {
                stringBuilder.append(");");
            } else {
                stringBuilder.append(" OR ");
            }
        }

        log.info("removeReceivers(): QUERY BUILT: '" + stringBuilder.toString()+"'");

        try {
            ps = newQuery(stringBuilder.toString());
            int receiversDeleted =ps.executeUpdate();
            log.debug("removeReceivers() number of receivers removed:" + receiversDeleted);
        } catch (SQLException e) {
            log.error("Error: ", e);
        } finally {
            endQuery(ps);
        }

        log.debug("removeReceivers() finished");
    }
}
