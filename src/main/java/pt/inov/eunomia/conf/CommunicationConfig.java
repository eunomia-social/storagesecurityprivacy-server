/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("communication")
public class CommunicationConfig {

    private int timeoutMillis;

    private int deliveryIntervalMillis;

    private int temporalCriteria;

    public int getTimeoutMillis() {
        return timeoutMillis;
    }

    public void setTimeoutMillis(int timeoutMillis) {
        this.timeoutMillis = timeoutMillis;
    }

    public int getDeliveryIntervalMillis() {
        return deliveryIntervalMillis;
    }

    public void setDeliveryIntervalMillis(int deliveryIntervalMillis) {
        this.deliveryIntervalMillis = deliveryIntervalMillis;
    }

    public int getTemporalCriteria() {
        return temporalCriteria;
    }

    public void setTemporalCriteria(int temporalCriteria) {
        this.temporalCriteria = temporalCriteria;
    }
}
