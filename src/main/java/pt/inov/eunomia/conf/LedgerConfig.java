/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * This class stores the configuration properties for the ledger of the Node.
 * @author lmss
 */
@Component
@ConfigurationProperties("ledger")
public class LedgerConfig {
    private String url;
    private int timeoutMillis;
    private boolean cacheEnabled;
    private int cachePollingPeriodMillis;
    private boolean disableSignature;
    
    /**
     * @return the URL
     */    
    public String getUrl() {
        return url;
    }

    /**
     * @param url the URL to set.
     */
    public void setUrl(String url) {
        this.url = url;
    }
        
    /**
     * @return the timeoutMillis
     */
    public int getTimeoutMillis() {
        return timeoutMillis;
    }

    /**
     * @param timeoutMillis the timeoutMillis to set
     */
    public void setTimeoutMillis(int timeoutMillis) {
        this.timeoutMillis = timeoutMillis;
    }
 
    /**
     * @return the cacheEnabled
     */
    public boolean isCacheEnabled() {
        return cacheEnabled;
    }

    /**
     * @param cacheEnabled the cacheEnabled to set
     */
    public void setCacheEnabled(boolean cacheEnabled) {
        this.cacheEnabled = cacheEnabled;
    }

    /**
     * @return the cachePollingPeriodMillis
     */
    public int getCachePollingPeriodMillis() {
        return cachePollingPeriodMillis;
    }

    /**
     * @param cachePollingPeriodMillis the cachePollingPeriodMillis to set
     */
    public void setCachePollingPeriodMillis(int cachePollingPeriodMillis) {
        this.cachePollingPeriodMillis = cachePollingPeriodMillis;
    }
   
    /**
     * If true, we will not use BlockChain to verify signatures.
     * @return the disableSignature
     */
    public boolean isDisableSignature() {
        return disableSignature;
    }

    /**
     * @param disableSignature the disableSignature to set
     */
    public void setDisableSignature(boolean disableSignature) {
        this.disableSignature = disableSignature;
    }
}
