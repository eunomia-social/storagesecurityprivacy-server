/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * This class stores the generic configuration properties of the Node.
 * @author lmss
 */
@Component
@ConfigurationProperties("node")
public class NodeConfig {
    private String id;
    private String description;
    private String pubKey;
    private String priKey;
    private String votingServerId;

    private String url;
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the pubKey
     */
    public String getPubKey() {
        return pubKey;
    }

    /**
     * @param pubKey the pubKey to set
     */
    public void setPubKey(String pubKey) {
        this.pubKey = pubKey;
    }

    /**
     * @return the priKey
     */
    public String getPriKey() {
        return priKey;
    }

    /**
     * @param priKey the priKey to set
     */
    public void setPriKey(String priKey) {
        this.priKey = priKey;
    }

    /**
     * @return the votingServerId
     */
    public String getVotingServerId() {
        return votingServerId;
    }

    /**
     * @param votingServerId the votingServerId to set
     */
    public void setVotingServerId(String votingServerId) {
        this.votingServerId = votingServerId;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the node url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
