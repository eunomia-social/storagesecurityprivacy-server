/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.model.tracking;

import com.fasterxml.jackson.annotation.JsonFormat;
import pt.inov.eunomia.util.Util;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import pt.inov.eunomia.model.LedgerEntryEntity;
import pt.inov.eunomia.model.ledger.LedgerEntry;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Logger Entry received by the Tracking API.
 *
 * The timestamp which is sent to the Ledger Service, in order to meet the requirements established
 * by the Ledger Service API, is going to be converted to the epoch format. As the size provided by the
 * API is only an int, and the epoch time being a long, we must divide the time by 1000 for it to fit
 * the space provided. Therefore, the time is going to be stored as seconds.
 */
@Validated
@ApiModel(description = "All details about the Entry")
public class TrackEntry implements LedgerEntryEntity<TrackEntry>, Serializable {

    private static final Logger log = LoggerFactory.getLogger(TrackEntry.class);

    public static final String TYPE = "Track";

    @ApiModelProperty(notes = "The id of the object")
    @JsonProperty("id")
    private String id;

    @ApiModelProperty(notes = "Reference to the node")
    @JsonProperty("node_reference")
    @NotNull(message = "The reference mustn't be null")
    @NotEmpty(message = "Please provide a node reference")
    private String nodeReference = null;

    @ApiModelProperty(notes = "The ID of the actor")
    @JsonProperty("actor_id")
    @NotNull(message = "The actor ID can't be null")
    @NotEmpty(message = "Please provide an actor id")
    private String actorId;

    @ApiModelProperty(notes = "The code of the action")
    @JsonProperty("action_code")
    @NotNull(message = "The action code can't be null")
    @NotEmpty(message = "Please provide an action code")
    private String actionCode;

    @ApiModelProperty(notes = "The description of the action")
    @JsonProperty("action_description")
    @NotNull(message = "The description can't be null")
    @NotEmpty(message = "Please provide an action description")
    private String actionDescription;

    @ApiModelProperty(notes = "The timestamp of the action")
    @JsonProperty("timestamp")
    @JsonFormat (shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    @NotNull(message = "The timestamp can't be null")
    private Date timestamp;

    @ApiModelProperty(notes = "The id of the ledger entry associated")
    @JsonProperty("ledger_entry_id")
    private int ledgerEntryId;
    
    @ApiModelProperty(notes = "If true it means the integrity of the returned information is validated by the Legdger. Please note that is attributed is only used when returned by the server.")
    @JsonProperty("validated")
    
    private boolean validated;

    public TrackEntry() {
    }

    public static String getTYPE() {
        return TYPE;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNodeReference() {
        return nodeReference;
    }

    public void setNodeReference(String nodeReference) {
        this.nodeReference = nodeReference;
    }

    public String getActorId() {
        return actorId;
    }

    public void setActorId(String actorId) {
        this.actorId = actorId;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getActionDescription() {
        return actionDescription;
    }

    public void setActionDescription(String actionDescription) {
        this.actionDescription = actionDescription;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public int getLedgerEntryId() {
        return ledgerEntryId;
    }

    public void setLedgerEntryId(int ledgerEntryId) {
        this.ledgerEntryId = ledgerEntryId;
    }
 
    @Override
    public boolean isValidated() {
        return validated;
    }
    
    @Override
    public void setValidated(boolean validated) {
        this.validated = validated;
    }
    
    @Override
    public LedgerEntry toLedgerEntry() {

        log.info("toLedgerEntry()");

        LedgerEntry newLedgerEntry;
        try {
            LedgerEntry ledgerEntry = new LedgerEntry();
            ledgerEntry.setId(this.getLedgerEntryId());
            ledgerEntry.setIpfs(this.getNodeReference());
            ledgerEntry.setTimestamp((int) (this.getTimestamp().getTime() / 1000));
            ledgerEntry.setSignature(Util.bytesToHex(Util.toHash(this)));
            ledgerEntry.setType(TYPE);
            newLedgerEntry=ledgerEntry;
        } catch (Exception e) {
            log.error("toLedgerEntry() Error:", e);
            newLedgerEntry=null;
        }

        log.info("toLedgerEntry() returning Ledger Entry");

        return newLedgerEntry;
    }

   
}
