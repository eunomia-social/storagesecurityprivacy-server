/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.model.storage;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import pt.inov.eunomia.model.ledger.LedgerEntry;
import pt.inov.eunomia.model.LedgerEntryEntity;
import pt.inov.eunomia.util.Util;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;


/**
 * This class represents the data that is received from EUNOMIA services and is stored
 * in orbitDb.
 */
@Validated
@ApiModel(description = "All details about the Eunomia Object")
public class StorageObject implements pt.inov.eunomia.model.StorageObject,
        LedgerEntryEntity<StorageObject>, Serializable {

    private static final Logger log = LoggerFactory.getLogger(StorageObject.class);

    public static final String TYPE = "Object";

    @ApiModelProperty(notes = "The object id")
    @JsonProperty("id")
    private String id;

    @ApiModelProperty(notes = "The object data")
    @JsonProperty("properties")
    @NotNull(message = "Object must have at least one property")
    private ObjectNode properties = null;

    @ApiModelProperty(notes = "The object type")
    @JsonProperty("type")
    @NotNull(message = "Object must have a type")
    private String type;

    @ApiModelProperty(notes = "The id of the ledger entry associated")
    @JsonProperty("ledger_entry_id")
    private int ledgerEntryId;
    
    private boolean validated;

    public StorageObject() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ObjectNode getProperties() {
        return properties;
    }

    public void setProperties(ObjectNode properties) {
        this.properties = properties;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLedgerEntryId() {
        return ledgerEntryId;
    }

    public void setLedgerEntryId(int ledgerEntryId) {
        this.ledgerEntryId = ledgerEntryId;
    }

    @Override
    public boolean isValidated() {
        return validated;
    }

    @Override
    public void setValidated(boolean validated) {
        this.validated = validated;
    }
    
    @Override
    public void fromStorage() {
    }

    @Override
    public void toStorage() {

    }

    @Override
    public LedgerEntry toLedgerEntry() {

        log.info("toLedgerEntry()");

        LedgerEntry ledgerEntry = null;
        try {
            ledgerEntry = new LedgerEntry();
            ledgerEntry.setId(this.getLedgerEntryId());
            // TODO: Set node reference using NodeService
            ledgerEntry.setIpfs("");
            Date date = new Date();
            ledgerEntry.setTimestamp((int) (date.getTime() / 1000));
            ledgerEntry.setSignature(Util.bytesToHex(Util.toHash(this)));
            ledgerEntry.setType(TYPE);
        } catch (Exception e) {
            log.error("toLedgerEntry() Error:", e);
        }

        log.info("toLedgerEntry() returning Ledger Entry");

        return ledgerEntry;
    }

  
}
