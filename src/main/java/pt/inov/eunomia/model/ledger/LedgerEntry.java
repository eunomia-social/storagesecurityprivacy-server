/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.model.ledger;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

/**
 * Represents a generic Ledger Entry.
 */
@Validated
@ApiModel(description = "Details of a Ledger Entry")
public class LedgerEntry {
    
    @ApiModelProperty(notes = "The entry ID")
    @JsonProperty("id")
    @NotNull(message = "The id of an entry can't be null")
    private int id;

    @ApiModelProperty(notes = "The timestamp of the entry creation")
    @JsonProperty("timestamp")
    @NotNull(message = "The timestamp of the entry creation can't be null")
    private int timestamp;

    @ApiModelProperty(notes = "The IPFS node reference where the entry belongs")
    @JsonProperty("ipfs")
    @NotNull(message = "The IPFS node reference can't be null")
    private String ipfs;

    @ApiModelProperty(notes = "The entry data signature")
    @JsonProperty("signature")
    @NotNull(message = "The signature can't be null")
    private String signature;

    @ApiModelProperty(notes = "The entry type")
    @JsonProperty("type")
    @NotNull(message = "The entry type can't be null")
    private String type;
    
    @JsonProperty(value="docType", required=false)
    private String docType;
   
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public String getIpfs() {
        return ipfs;
    }

    public void setIpfs(String ipfs) {
        this.ipfs = ipfs;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "LedgerEntry{" +
                "id=" + id +
                ", timestamp=" + timestamp +
                ", ipfs='" + ipfs + '\'' +
                ", signature='" + signature + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    /**
     * @return the docType
     */
    public String getDocType() {
        return docType;
    }

    /**
     * @param docType the docType to set
     */
    public void setDocType(String docType) {
        this.docType = docType;
    }
}
