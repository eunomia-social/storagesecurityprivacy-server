/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.model.communication;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.validation.annotation.Validated;

@Validated
@ApiModel(description = "Object representing the service to receive the message")
public class Receiver {

    @ApiModelProperty(notes = "Id of the node to which the message is to be sent to.")
    @JsonProperty("node_id")
    @NotBlank(message = "Please provide the node id.")
    private String nodeId;

    @ApiModelProperty(notes = "Id of the service to which the message is to be sent to.")
    @JsonProperty("service_id")
    @NotBlank(message = "Please provide the service id.")
    private String serviceId;

    public Receiver() {
    }

    public Receiver(String nodeId, String serviceId) {
        this.nodeId = nodeId;
        this.serviceId = serviceId;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
}
