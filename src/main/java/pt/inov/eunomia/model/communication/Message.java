/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.model.communication;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotBlank;
import org.msgpack.core.annotations.Nullable;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;

@Validated
@ApiModel(description = "Message object sent between nodes.")
public class Message {

    private static final String STATUS_PENDING = "Pending";

    private static final String STATUS_SENT = "Sent";

    private long id;

    @ApiModelProperty(notes = "Sender reference of the message sent.")
    @JsonProperty("sender")
    @NotBlank(message = "Please provide the sender.")
    private String sender;

    @ApiModelProperty(notes = "Receiver reference of the message sent.")
    @JsonProperty("receivers")
    private ArrayList<Receiver> receivers;

    @ApiModelProperty(notes = "Content of the message")
    @JsonProperty("content")
    @NotBlank(message = "Please provide the content.")
    private String content;

    private String status;

    private long timestamp;

    public Message() {
        this.status = STATUS_PENDING;
        this.timestamp = System.currentTimeMillis();
        this.receivers = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public ArrayList<Receiver> getReceivers() {
        return receivers;
    }

    public void setReceivers(ArrayList<Receiver> receivers) {
        this.receivers = receivers;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setAsPending() {
        this.status = STATUS_PENDING;
    }

    public void setAsSent() {
        this.status = STATUS_SENT;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
