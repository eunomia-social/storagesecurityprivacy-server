/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.model.communication;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.validation.annotation.Validated;

@Validated
@ApiModel(description = "Details of a service to be bound.")
public class Service {

    @ApiModelProperty(notes = "Identifier of the service.")
    @JsonProperty("id")
    @NotBlank(message = "Please provide the service's identifier.")
    private String id;

    @ApiModelProperty(notes = "Name of the service.")
    @JsonProperty("name")
    @NotBlank(message = "Please provide the service's name.")
    private String name;

    @ApiModelProperty(notes = "Endpoint to be called when a message is directed to this service.")
    @JsonProperty("callback")
    @NotBlank(message = "Please provide the service's callback.")
    private String callback;

    public Service() {
    }

    public Service(String id, String name, String callback) {
        this.id = id;
        this.name = name;
        this.callback = callback;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCallback() {
        return callback;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }
}
