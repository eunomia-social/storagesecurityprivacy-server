/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.model.node;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

/**
 *
 * @author lmss
 */
@Validated
@ApiModel(description = "All information details about the Node")
@JsonPropertyOrder({"id", "description","pubKey"})
public class NodeInformation {
    @ApiModelProperty(notes = "The node ID")
    @JsonProperty("id")
    @NotNull(message = "The id of a node can't be null")
    private String id;
    @ApiModelProperty(notes = "The node description. This is a free text to give some overview about this node instance.")
    @JsonProperty("description")
    private String description;
    @ApiModelProperty(notes = "The node public key. This is the node public key. This information can also be located in the discovery API.")
    @JsonProperty("pubKey")
    private String pubKey;
    @JsonIgnore
    private String privKey;
    @JsonProperty("votingServiceId")
    private String votingServiceId;    
    

    @ApiModelProperty(notes = "The node's host IP to be used to locate the services residing on this node.")
    @JsonProperty("nodeUrl")
    private String nodeUrl;
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the pubKey
     */
    public String getPubKey() {
        return pubKey;
    }

    /**
     * @param pubKey the pubKey to set
     */
    public void setPubKey(String pubKey) {
        this.pubKey = pubKey;
    }

    /**
     * @return the privKey
     */
    public String getPrivKey() {
        return privKey;
    }

    /**
     * @param privKey the privKey to set
     */
    public void setPrivKey(String privKey) {
        this.privKey = privKey;
    }    

    /**
     * @return the votingServiceId
     */
    public String getVotingServiceId() {
        return votingServiceId;
    }

    /**
     * @param votingServiceId the votingServiceId to set
     */
    public void setVotingServiceId(String votingServiceId) {
        this.votingServiceId = votingServiceId;
    }

    /**
     * @return the nodeUrl
     */
    public String getNodeUrl() {
        return nodeUrl;
    }

    /**
     * @param nodeUrl the nodeIp to set
     */
    public void setNodeUrl(String nodeUrl) {
        this.nodeUrl = nodeUrl;
    }
}
