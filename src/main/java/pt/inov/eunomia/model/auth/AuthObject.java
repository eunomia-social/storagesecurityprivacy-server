/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.model.auth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import pt.inov.eunomia.model.ledger.LedgerEntry;
import pt.inov.eunomia.model.LedgerEntryEntity;
import pt.inov.eunomia.util.Util;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Validated
@ApiModel(description = "Represents the authentication object retrieved to the users")
public class AuthObject implements LedgerEntryEntity<AuthObject>, Serializable {

    private static final Logger log = LoggerFactory.getLogger(AuthObject.class);

    public static final String TYPE = "Auth";

    @ApiModelProperty(notes = "The EUNOMIA access token")
    @JsonProperty("token")
    @NotNull(message = "The EUNOMIA access token can't be null")
    private String token;

    @ApiModelProperty(notes = "Timestamp of the access token creation date")
    @JsonProperty("created_at")
    private Date createdAt;

    @ApiModelProperty(notes = "Timestamp of the access token expiration date")
    @JsonProperty("expired_at")
    private Date expiredAt;

    @ApiModelProperty(notes = "Flag to state if a token is revoked")
    @JsonProperty("revoked")
    private boolean revoked;

    @ApiModelProperty(notes = "The id of the ledger entry associated")
    @JsonProperty("ledger_entry_id")
    private int ledgerEntryId;

    @ApiModelProperty(notes = "The id of the user associated")
    @JsonProperty("user_id")
    private String userId;
    
    private boolean validated;

    
    public AuthObject() {
        this.revoked = false;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @JsonIgnore
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @JsonIgnore
    public Date getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(Date expiredAt) {
        this.expiredAt = expiredAt;
    }

    @JsonIgnore
    public boolean isRevoked() {
        return revoked;
    }

    public void setRevoked(boolean revoked) {
        this.revoked = revoked;
    }

    public int getLedgerEntryId() {
        return ledgerEntryId;
    }

    public void setLedgerEntryId(int ledgerEntryId) {
        this.ledgerEntryId = ledgerEntryId;
    }

    public boolean checkTokenExpired() {
        return this.expiredAt.before(new Date());
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    @Override
    public LedgerEntry toLedgerEntry() {

        log.info("toLedgerEntry()");

        LedgerEntry ledgerEntry = null;
        try {
            ledgerEntry = new LedgerEntry();
            ledgerEntry.setId(this.getLedgerEntryId());
            // TODO: Set node reference using NodeService
            ledgerEntry.setIpfs("");
            Date date = new Date();
            ledgerEntry.setTimestamp((int) (date.getTime() / 1000));
            ledgerEntry.setSignature(Util.bytesToHex(Util.toHash(this)));
            ledgerEntry.setType(TYPE);
        } catch (Exception e) {
            log.error("toLedgerEntry() Error:", e);
        }

        log.info("toLedgerEntry() returning Ledger Entry");

        return ledgerEntry;
    }

    /**
     * @return the validated
     */
    public boolean isValidated() {
        return validated;
    }

    /**
     * @param validated the validated to set
     */
    public void setValidated(boolean validated) {
        this.validated = validated;
    }
}
