/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.model.discovery;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.inov.eunomia.model.ledger.LedgerEntry;
import pt.inov.eunomia.model.LedgerEntryEntity;
import pt.inov.eunomia.util.Util;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashMap;

/**
 * This class represents a discoverable entity.
 *
 * @author lmss
 */
public class Entity implements LedgerEntryEntity<Entity> {

    private static final Logger log = LoggerFactory.getLogger(Entity.class);

    public static final String TYPE = "DiscoveryEntity";

    /**
     * The entity ID.
     */
    @ApiModelProperty(notes = "The object id")
    @JsonProperty("id")
    @NotNull
    private String id;

    /**
     * Use this type for EUNOMIA nodes meta information.
     */
    public static String NODE_TYPE = "node";

    /**
     * Use this type for services;
     */
    public static String SERVICE_TYPE = "service";
    /**
     * This is the entity type. It can be a EUNOMIA node, but it can be also any
     * given service.
     */
    @ApiModelProperty(notes = "The entity type")
    @JsonProperty("type")
    @NotNull
    private String type;

    @ApiModelProperty
    @JsonProperty("properties")
    private HashMap<String, String> properties;

    /**
     * This last modified time stamp.
     */
    private Date lastModified = null;

    @ApiModelProperty(notes = "The id of the ledger entry associated")
    @JsonProperty("ledger_entry_id")
    private int ledgerEntryId;

    @JsonIgnore
    private boolean validated;

    public Entity() {
        this.properties = new HashMap<>();
        this.lastModified = new Date();
    }

    /**
     * @return The type.
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the properties
     */
    public HashMap<String, String> getProperties() {
        return properties;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties(HashMap<String, String> properties) {
        this.properties = properties;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "[" + id + "," + properties + "]";
    }

    /**
     * @return the lastModified
     */
    public Date getLastModified() {
        return lastModified;
    }

    /**
     * @param lastModified the lastModified to set
     */
    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public int getLedgerEntryId() {
        return ledgerEntryId;
    }

    public void setLedgerEntryId(int ledgerEntryId) {
        this.ledgerEntryId = ledgerEntryId;
    }

    @Override
    public LedgerEntry toLedgerEntry() {

        log.info("toLedgerEntry()");

        LedgerEntry ledgerEntry = null;
        try {
            ledgerEntry = new LedgerEntry();
            ledgerEntry.setId(this.getLedgerEntryId());
            // TODO: Set node reference using NodeService
            ledgerEntry.setIpfs("");
            Date date = new Date();
            ledgerEntry.setTimestamp((int) (date.getTime() / 1000));
            ledgerEntry.setSignature(Util.bytesToHex(Util.toHash(this)));
            ledgerEntry.setType(TYPE);
        } catch (Exception e) {
            log.error("toLedgerEntry() Error:", e);
        }

        log.info("toLedgerEntry() returning Ledger Entry");

        return ledgerEntry;
    }

    /**
     * @return the validated
     */
    public boolean isValidated() {
        return validated;
    }

    /**
     * @param validated the validated to set
     */
    public void setValidated(boolean validated) {
        this.validated = validated;
    }
}
