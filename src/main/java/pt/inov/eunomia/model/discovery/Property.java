/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.model.discovery;

/**
 * The Entity properties.
 * @author lmss
 */
public class Property {
    public static String NAME_NODE_ADDRESS="NODE_ADDRESS";
    public static String NAME_NODE_PUB_KEY="NODE_PUBKEY";
    public static String NAME_NODE_DESCRIPTION="NODE_DESCRIPTION";
    public static String NAME_NODE_URL = "NODE_URL";
    public static String NAME_VOTING_SERVER_ID="VOTING_SERVER_ID";
  
    private String name;
    private String value;
    
    public Property(){}
    
    public Property(String name, String value){
        this.name=name;
        this.value=value;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
    
    @Override
    public String toString(){
        return "["+name+","+value+"]";
    }
}
