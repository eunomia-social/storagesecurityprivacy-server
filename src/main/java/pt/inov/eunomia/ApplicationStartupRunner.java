/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import pt.inov.eunomia.api.filters.HTTPRequestEnablerFilter;
import pt.inov.eunomia.db.communication.Inbox;
import pt.inov.eunomia.db.communication.Outbox;
import pt.inov.eunomia.db.communication.ServicePersistence;
import pt.inov.eunomia.db.orbitdb.OrbitDbManager;
import pt.inov.eunomia.services.communication.CommunicationService;
import pt.inov.eunomia.services.ledger.LedgerIntegrityService;
import pt.inov.eunomia.services.node.NodeService;

@Component
public class ApplicationStartupRunner implements ApplicationRunner {

    private static final Logger log = LoggerFactory.getLogger(ApplicationStartupRunner.class);

    @Autowired
    private OrbitDbManager orbitDbManager;

    @Autowired
    private NodeService nodeService;

    @Autowired
    private LedgerIntegrityService ledgerIntegrityService;

    @Autowired
    private ServicePersistence servicePersistence;

    @Autowired
    private Inbox inbox;

    @Autowired
    private Outbox outbox;

    @Autowired
    private CommunicationService communicationService;

    @Autowired
    private HTTPRequestEnablerFilter httpRequestEnablerFilter;

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        log.info("run() Booting up!");
        log.info("run() Creating/Recreating service tables for the communication service ...");
        this.servicePersistence.createTable();
        log.info("run() Creating/Recreating Inbox for the communication service ...");
        this.inbox.createInbox();
        log.info("run() Creating/Recreating Outbox for the communection service ...");
        this.outbox.createOutbox();
        log.info("run() Creating/Reconnecting OrbitDB databases...");
        this.orbitDbManager.initialize();
        log.info("run() Sleeping 1 second for orbitdb settling up...");
        Thread.sleep(1000);
        log.info("run() Initializing Ledger Integrity Service...");
        this.ledgerIntegrityService.initialize();
        log.info("run() Initializing Communication Service...");
        this.communicationService.initialize();
        log.info("run() Initializing Node Service...");
        this.nodeService.initialize();
        log.info("run() Enabling HTTP processing!");
        httpRequestEnablerFilter.setEnabled(true);
        log.info("run() Finished up running!");
    }
}
