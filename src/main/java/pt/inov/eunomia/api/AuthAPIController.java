/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pt.inov.eunomia.api.responses.ApiError;
import pt.inov.eunomia.api.responses.ApiResponseMessage;
import pt.inov.eunomia.services.auth.AuthService;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import pt.inov.eunomia.conf.SNConfig;

@RestController
public class AuthAPIController implements AuthAPI {

    private static final Logger log = LoggerFactory.getLogger(AuthAPIController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private AuthService authService;
    @Autowired
    private SNConfig snConfig;
    
    @org.springframework.beans.factory.annotation.Autowired
    public AuthAPIController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Object> getEunomiaAccessToken(
            @ApiParam(value = "Mastodon Access Token retrieved by Mastodon's OAuth API",required=true)
            @RequestParam("access_token") String accessToken,
            @ApiParam(value = "Uri of the authentication provider to verify credentials")
            @RequestParam("auth_provider_uri") String authProviderUri) {

        if(snConfig.getForceMastodonProvider()!=null&&snConfig.getForceMastodonProvider().length()>0){            
            log.info("getEunomiaAccessToken() is configured to use a fixed SN provider:"+snConfig.getForceMastodonProvider() +" so ignored the received one:"+authProviderUri);
            authProviderUri=snConfig.getForceMastodonProvider();
        }else{
            log.info("getEunomiaAccessToken() from received provider:"+authProviderUri);
        }
        
        ObjectNode userData = this.authService.verifyAccountWithDsn(accessToken, authProviderUri);
        
        if (userData == null) {
            ApiError error = new ApiError(HttpStatus.UNAUTHORIZED, "Unauthorized Access",
                    "The mastodon token" + accessToken + " is invalid in the " +
                            authProviderUri + " server");
            log.error("getEunomiaAccessToken() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }
        //String userId=userData.get("id").asText();
        ObjectNode userCreated = this.authService.createAccount(userData);
        if (userCreated == null) {
            ApiError error = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Error creating account or " +
                    "ledger entry associated.");
            log.error("getEunomiaAccessToken() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }
        log.info("getEunomiaAccessToken() User created OK:"+userCreated);
        String userId=userCreated.get("properties").get("id").asText();
        String token = this.authService.generateToken(userId);
        if (token == null) {
            ApiError error = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Error creating the access " +
                    "token or ledger entry associated.");
            log.error("getEunomiaAccessToken() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }
        log.info("getEunomiaAccessToken() User token created OK");
        ObjectNode resData = objectMapper.createObjectNode();
        resData.put("token", token);
        resData.put("object", userCreated);
        
        //resData.put("userId", )
        ApiResponseMessage responseMessage = new ApiResponseMessage(HttpStatus.OK, "Authentication granted", resData);

        log.info("getEunomiaAccessToken() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(responseMessage, new HttpHeaders(), responseMessage.getStatus());
    }

    @Override
    public ResponseEntity<Object> validateEunomiaToken(
            @ApiParam(value = "Eunomia Token retrieved before", required = true)
            @RequestParam("access_token") String accessToken, 
            @ApiParam(value = "User ID for the EUNOMIA", required = false)
            @RequestParam(value="user_id", required = false)
            String userId) {

        log.info("validateEunomiaToken()");

        ApiError error = new ApiError(HttpStatus.UNAUTHORIZED, "Invalid Token");

        if (!this.authService.tokenIsValid(accessToken, userId)) {
            error.setErrors(Arrays.asList("The access token may have expired or been revoked."));
            log.error("validateEunomiaToken() Error: The access token may have expired or been revoked.");
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }

        ApiResponseMessage responseMessage = new ApiResponseMessage(HttpStatus.OK, "Valid token");

        log.info("validateEunomiaToken() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(responseMessage, new HttpHeaders(), responseMessage.getStatus());
    }

    @Override
    public ResponseEntity<Object> revokeEunomiaToken(
            @ApiParam(value = "Eunomia Access Token retrieved before",required=true)
            @RequestParam("access_token") String accessToken) {

        log.info("revokeEunomiaToken()");

        ApiError error = new ApiError(HttpStatus.UNAUTHORIZED, "Invalid Token");

        if (!this.authService.tokenIsValid(accessToken,null)) {
            error.setErrors(Arrays.asList("The access token may have expired or been revoked."));
            log.error("revokeEunomiaToken() Error: The access token may have expired or been revoked.");
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }

        this.authService.revokeToken(accessToken);
        ApiResponseMessage responseMessage = new ApiResponseMessage(HttpStatus.OK, "Token revoked");

        log.info("revokeEunomiaToken() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(responseMessage, new HttpHeaders(), responseMessage.getStatus());
    }

}
