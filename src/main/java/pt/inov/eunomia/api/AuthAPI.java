/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.api;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Api(value = "eunomia", description = "the eunomia API")
public interface AuthAPI {

    @ApiOperation(value = "Get Eunomia Access Token by providing the Mastodon Access Token",
            nickname = "eunomiaTokenAccessTokenGet", notes = "", response = Object.class, tags={ "Authentication Service", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Eunomia Access Token successfully retrieved", response = Object.class),
        @ApiResponse(code = 400, message = "Invalid Operation", response = Object.class),
        @ApiResponse(code = 401, message = "Unauthorized access to perform the operation") })
    @GetMapping(value = "/eunomia/token",
        produces = { "application/json" })
    ResponseEntity<Object> getEunomiaAccessToken(
            @ApiParam(value = "Mastodon Access Token retrieved by Mastodon's OAuth API", required = true)
            @RequestParam("access_token") String accessToken,
            @ApiParam(value = "Uri of the authentication provider", required = true)
            @RequestParam("auth_provider_uri") String authProviderUri);

    @ApiOperation(value = "Validate Eunomia Access Token", nickname = "eunomiaTokenAccessTokenValidate", notes = "", response = Object.class, tags={ "Authentication Service", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Eunomia Access Token is valid", response = Object.class),
            @ApiResponse(code = 401, message = "Unauthorized access") })
    @PostMapping(value = "/eunomia/token",
            produces = { "application/json" })
    ResponseEntity<Object> validateEunomiaToken(
            @ApiParam(value = "Eunomia Token retrieved before", required = true)
            @RequestParam("access_token") String accessToken, 
            @ApiParam(value = "User ID for the EUNOMIA", required = false)
            @RequestParam(value="user_id", required = false)
            String userId);

    @ApiOperation(value = "Revoke Eunomia Access Token", nickname = "eunomiaTokenAccessTokenRevoke", notes = "", response = Object.class, tags={ "Authentication Service", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Eunomia Access Token Revoked", response = Object.class),
            @ApiResponse(code = 401, message = "Unauthorized access") })
    @PostMapping(value = "/eunomia/token/revoke",
            produces = { "application/json" })
    ResponseEntity<Object> revokeEunomiaToken(
            @ApiParam(value = "Eunomia Token retrieved before", required = true)
            @RequestParam("access_token") String accessToken);
}
