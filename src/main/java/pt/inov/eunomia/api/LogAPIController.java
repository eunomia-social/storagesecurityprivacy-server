/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.api;

import io.swagger.annotations.ApiParam;
import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lmss
 */
@RestController
public class LogAPIController implements LogAPI{
    private static Logger logger=Logger.getLogger(LogAPIController.class);
    
    @Override
    public ResponseEntity<Object> log( 
            @ApiParam(value = "Eunomia Access Token", required = true)
            @RequestParam("access_token") String accessToken, 
            @RequestParam(value = "Log Category", required = true) String category,
            @ApiParam(value ="Log Line") 
            @RequestBody
            String line) {
        logger.info("category:"+category+"-"+line);
        return ResponseEntity.ok(null);
    }
    
}
