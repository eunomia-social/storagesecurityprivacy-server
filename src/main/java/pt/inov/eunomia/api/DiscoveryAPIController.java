/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pt.inov.eunomia.api.responses.ApiError;
import pt.inov.eunomia.api.responses.ApiResponseMessage;
import pt.inov.eunomia.model.discovery.Entity;
import pt.inov.eunomia.services.discovery.DiscoveryService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
public class DiscoveryAPIController implements DiscoveryAPI {

    private static final Logger log = LoggerFactory.getLogger(DiscoveryAPIController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private DiscoveryService discoveryService;

    public DiscoveryAPIController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Override
    public ResponseEntity<Object> create(
            @ApiParam(value = "Entity to send on the request" ,required=true )
            @Valid @RequestBody Entity entity,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure",required=true)
            @RequestParam("access_token") String accessToken) {

        log.info("create()");

        entity = this.discoveryService.registerEntity(entity);

        if (entity == null) {
            ApiError error = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to create Entity");
            log.error("create() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }

        ObjectNode rootNode = objectMapper.createObjectNode();
        ObjectNode objectCreated = this.objectMapper.valueToTree(entity);
        rootNode.put("entity", objectCreated);

        ApiResponseMessage responseMessage = new ApiResponseMessage(
                HttpStatus.OK,
                "Entity successfully created",
                rootNode
        );

        log.info("create() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(
                responseMessage,
                new HttpHeaders(),
                responseMessage.getStatus()
        );
    }

    @Override
    public ResponseEntity<Object> list(
            @ApiParam(value = "Name of the filter property" ,required=true )
            @RequestParam("property") String[] properties,
            @ApiParam(value = "Comparison operator to apply" ,required=true )
            @RequestParam("comp") String[] comps,
            @ApiParam(value = "Value of the filter property" ,required=true )
            @RequestParam("value") String[] values,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure",required=true)
            @RequestParam("access_token") String accessToken) {

        log.info("list()");

        if (properties.length != values.length || comps.length != values.length) {
            ApiError error = new ApiError(HttpStatus.BAD_REQUEST,
                    "The properties/comparisons/values don't match");
            log.info("list() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }

        ArrayNode entities = this.discoveryService.list(properties, comps, values);

        if (entities == null) {
            ApiError error = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to list entities");
            log.error("list() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }

        String message = "Entities successfully listed";

        if (entities.size() == 0) {
            message = "No entities found";
        }

        ObjectNode rootNode = objectMapper.createObjectNode();
        rootNode.put("entities", entities);

        ApiResponseMessage responseMessage = new ApiResponseMessage(
                HttpStatus.OK,
                message,
                rootNode
        );

        log.info("list() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(
                responseMessage,
                new HttpHeaders(),
                responseMessage.getStatus()
        );
    }

    public ResponseEntity<Object> update(
            @ApiParam(value = "Entity to send on the request" ,required=true )
            @Valid @RequestBody Entity body,
            @ApiParam(value = "Id of the entity specified",required=true)
            @PathVariable("entityId") String entityId,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure",required=true)
            @RequestParam("access_token") String accessToken) {

        log.info("update() with id: " + entityId);

        Entity entity = this.discoveryService.get(entityId);
        if (entity == null) {
            ApiError error = new ApiError(HttpStatus.NOT_FOUND, "Couldn't find an object with " +
                    "the id provided");
            log.error("update() error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }

        Entity newEntity = this.discoveryService.update(body, entity);
        if (newEntity == null) {
            ApiError error = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Unable to create a Ledger Entry");
            log.error("update() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }

        ObjectNode objectUpdated = this.objectMapper.valueToTree(newEntity);

        ObjectNode rootNode = objectMapper.createObjectNode();
        rootNode.put("entity", objectUpdated);

        ApiResponseMessage responseMessage = new ApiResponseMessage(
                HttpStatus.OK,
                "Entity successfully updated",
                rootNode);

        log.info("update() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(responseMessage, new HttpHeaders(), responseMessage.getStatus());
    }

    @Override
    public ResponseEntity<Object> delete(
            @ApiParam(value = "Id of the entity specified",required=true)
            @PathVariable("entityId") String entityId,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure",required=true)
            @RequestParam("access_token") String accessToken) {

        log.info("delete() with id: " + entityId);

        this.discoveryService.unregister(entityId);

        ApiResponseMessage responseMessage = new ApiResponseMessage(
                HttpStatus.OK,
                "Entity successfully deleted"
        );

        log.info("delete() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(
                responseMessage,
                new HttpHeaders(),
                responseMessage.getStatus()
        );
    }
}
