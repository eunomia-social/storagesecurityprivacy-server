/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.ApiParam;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pt.inov.eunomia.api.responses.ApiError;
import pt.inov.eunomia.api.responses.ApiResponseMessage;
import pt.inov.eunomia.model.tracking.TrackEntry;
import pt.inov.eunomia.services.ledger.LedgerIntegrityService;
import pt.inov.eunomia.services.track.TrackingService;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import pt.inov.eunomia.model.LedgerEntryEntity;

@RestController
public class TrackingAPIController implements TrackingAPI {

    private static final Logger log = LoggerFactory.getLogger(TrackingAPIController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private TrackingService trackingService;

    @Autowired
    private LedgerIntegrityService ledgerIntegrityService;

    @Autowired
    public TrackingAPIController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Override
    public ResponseEntity<Object> create(
            @ApiParam(value = "Entry details" ,required=true )
            @Valid @RequestBody TrackEntry trackEntry,
            @ApiParam(value = "Eunomia Access Token", required = true)
            @RequestParam("access_token") String accessToken) {

        log.info("create()");

        TrackEntry createdTrackEntry = this.trackingService.createEntry(trackEntry);

        if (createdTrackEntry == null) {
            ApiError error = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Unable to create a Ledger Entry");
            log.error("create() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }

        ObjectNode rootNode = this.objectMapper.createObjectNode();
        ObjectNode objectCreated = this.objectMapper.valueToTree(createdTrackEntry);
        rootNode.put("object", objectCreated);

        ApiResponseMessage responseMessage = new ApiResponseMessage(
                HttpStatus.OK,
                "Track created successfully",
                rootNode);

        log.info("create() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(responseMessage, new HttpHeaders(), responseMessage.getStatus());
    }

    @Override
    public ResponseEntity<Object> get(
            @ApiParam(value = "Eunomia Access Token", required = true)
            @RequestParam("access_token") String accessToken,
            @ApiParam(value = "Entry ID", required = true)
            @PathVariable("entry_id") String entryId) {

        log.info("get() with id: " + entryId);

        TrackEntry trackEntry = this.trackingService.getEntry(entryId);
        if (trackEntry == null) {
            ApiError error = new ApiError(HttpStatus.NOT_FOUND, "Couldn't find a track entry with " +
                    "the id provided");
            log.error("get() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }
        
        trackEntry.setValidated(false);
        this.ledgerIntegrityService.verifySignature(trackEntry);

        ObjectNode rootNode = this.objectMapper.createObjectNode();
        ObjectNode objectFetched = this.objectMapper.valueToTree(trackEntry);
        rootNode.put("entry", objectFetched);
        

        ApiResponseMessage responseMessage = new ApiResponseMessage(
                HttpStatus.OK,
                "Track created successfully",
                rootNode);

        log.info("get() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(responseMessage, new HttpHeaders(), responseMessage.getStatus());
    }

    @Override
    public ResponseEntity<Object> list(
            @ApiParam(value = "Name of the property to filter track entries by",required=true)
            @RequestParam("property") String[] properties,
            @ApiParam(value = "Comparison operator to apply",required=true)
            @RequestParam("comp") String[] comps,
            @ApiParam(value = "Value of the property",required=true)
            @RequestParam("value") String[] values,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure",required=true)
            @RequestParam("access_token") String accessToken) {

        log.info("list()");

        if (properties.length != values.length || comps.length != values.length) {
            ApiError error = new ApiError(HttpStatus.BAD_REQUEST,
                    "The properties/comparisons/values don't match");
            log.error("list() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }

        ArrayNode entries = this.trackingService.list(properties, comps, values);

        if (entries == null) {
            ApiError error = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to list track entries");
            log.error("list() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }
        
        String message="";
        ArrayNode validateObjects = this.objectMapper.createArrayNode();
        if (entries.size() > 0) {        
            ArrayList<LedgerEntryEntity> objsToValidate = new ArrayList<>();
            for (int i = 0; i < entries.size(); i++) {
                JsonNode node = entries.get(i);
                TrackEntry trackEntryUpdated = null;
                try {
                    trackEntryUpdated = this.objectMapper.treeToValue(node, TrackEntry.class);
                } catch (JsonProcessingException e) {
                    log.error("updateObject() Error: ", e);
                    return null;
                }
                objsToValidate.add(trackEntryUpdated);
            }

            ledgerIntegrityService.verifySignatures(objsToValidate);

            for (LedgerEntryEntity ledgerEntryEntity : objsToValidate) {
                TrackEntry storageObjectUpdated = (TrackEntry) ledgerEntryEntity;
                validateObjects.add(this.objectMapper.valueToTree(storageObjectUpdated));
            }
            message = "Entries successfully listed";
        }else {
            message = "No entries found";
        }

        ApiResponseMessage responseMessage = new ApiResponseMessage(
                HttpStatus.OK,
                message,
                validateObjects);

        log.info("list() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(responseMessage, new HttpHeaders(), responseMessage.getStatus());
    }
}
