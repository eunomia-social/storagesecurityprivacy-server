/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pt.inov.eunomia.api.responses.ApiError;
import pt.inov.eunomia.api.responses.ApiResponseMessage;
import pt.inov.eunomia.model.communication.Message;
import pt.inov.eunomia.model.communication.Service;
import pt.inov.eunomia.services.communication.CommunicationService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;

@RestController
public class CommunicationAPIController implements CommunicationAPI {

    private static final Logger log = LoggerFactory.getLogger(CommunicationAPIController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private CommunicationService communicationService;

    public CommunicationAPIController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Override
    public ResponseEntity<Object> getInbox(
            @ApiParam(value = "Id of the service inbox to be retrieved", required = true)
            @RequestParam("service_id") String serviceId,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure", required = true)
            @RequestParam("access_token") String accessToken) {

        log.info("Getting inbox messages");

        ArrayList<Message> messages = this.communicationService.getInbox(serviceId);
        ArrayNode messagesNode = objectMapper.createArrayNode();

        for (Message message : messages) {
            ObjectNode messageNode = this.objectMapper.valueToTree(message);
            messagesNode.add(messageNode);
        }

        ObjectNode resData = objectMapper.createObjectNode();
        resData.put("messages", messagesNode);

        ApiResponseMessage responseMessage = new ApiResponseMessage(HttpStatus.OK, "Messages successfully retrieved.", resData);

        log.info("getInbox() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(responseMessage, new HttpHeaders(), responseMessage.getStatus());
    }

    @Override
    public ResponseEntity<Object> sendMessage(
            @ApiParam(value = "Entity to send on the request", required = true )
            @Valid @RequestBody Message message,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure", required = true)
            @RequestParam("access_token") String accessToken) {

        log.info("sendMessage() init");

        boolean created = this.communicationService.sendMessage(message);

        if (! created) {
            ApiError error = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Error creating message");
            log.error("sendMessage() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }

        ObjectNode resData = objectMapper.createObjectNode();
        ObjectNode messageNode = this.objectMapper.valueToTree(message);
        resData.put("message", messageNode);

        ApiResponseMessage responseMessage = new ApiResponseMessage(HttpStatus.OK, "Message successfully sent.", resData);

        log.info("sendMessage() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(responseMessage, new HttpHeaders(), responseMessage.getStatus());
    }

    @Override
    public ResponseEntity<Object> receiveMessage(
            @ApiParam(value = "Message to be forwarded to the respective service.", required = true )
            @Valid @RequestBody Message message) {

        log.info("receiveMessage() init");

        boolean created = this.communicationService.receiveMessage(message);

        if (! created) {
            ApiError error = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Error creating message");
            log.error("receiveMessage() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }

        ApiResponseMessage responseMessage = new ApiResponseMessage(HttpStatus.OK, "Message successfully received.", null);

        log.info("receiveMessage() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(responseMessage, new HttpHeaders(), responseMessage.getStatus());
    }

    @Override
    public ResponseEntity<Object> bind(
            @ApiParam(value = "Service to be registered.", required = true)
            @RequestBody Service service,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure", required = true)
            @RequestParam("access_token") String accessToken) {

        log.info("bind() init");

        Service serviceCreated = this.communicationService.bind(service);

        if (serviceCreated == null) {
            ApiError error = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Error binding the service");
            log.error("bind() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }

        ObjectNode resData = objectMapper.createObjectNode();
        ObjectNode serviceNode = this.objectMapper.valueToTree(serviceCreated);
        resData.put("service", serviceNode);

        ApiResponseMessage responseMessage = new ApiResponseMessage(HttpStatus.OK, "Service successfully bound.", resData);

        log.info("bind() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(responseMessage, new HttpHeaders(), responseMessage.getStatus());
    }

    @Override
    public ResponseEntity<Object> unbind(
            @ApiParam(value = "Id of the service to be unbound", required = true)
            @RequestParam("service_id") String serviceId,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure", required = true)
            @RequestParam("access_token") String accessToken) {

        log.info("unbind() init");

        this.communicationService.unbind(serviceId);

        ApiResponseMessage responseMessage = new ApiResponseMessage(HttpStatus.OK, "Service successfully unbound.", null);

        log.info("unbind() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(responseMessage, new HttpHeaders(), responseMessage.getStatus());
    }
}
