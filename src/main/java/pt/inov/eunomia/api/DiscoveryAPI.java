/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.api;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.inov.eunomia.model.discovery.Entity;

import javax.validation.Valid;

@Api(value = "discovery", description = "The Eunomia Discovery API. Provides methods to discover other nodes.")
public interface DiscoveryAPI {

    @ApiOperation(value = "Create an entity", nickname = "createEntity", notes = "",
            tags={ "Discovery Service", },
            response = Entity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Entity successfully created"),
            @ApiResponse(code = 400, message = "Provided data is invalid"),
            @ApiResponse(code = 401, message = "Unauthorized access to perform the operation") })
    @PostMapping(value = "/discovery",
            consumes = { "application/json" })
    ResponseEntity<Object> create(
            @ApiParam(value = "Entity to send on the request" ,required=true )
            @Valid @RequestBody Entity entity,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure",required=true)
            @RequestParam("access_token") String accessToken);

    @ApiOperation(value = "List entities", nickname = "listEntities", notes = "",
            tags={ "Discovery Service", },
            response = Entity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Entities successfully retrieved"),
            @ApiResponse(code = 400, message = "Provided data is invalid"),
            @ApiResponse(code = 401, message = "Unauthorized access to perform the operation") })
    @GetMapping(value = "/discovery")
    ResponseEntity<Object> list(
            @ApiParam(value = "Name of the filter property" ,required=true )
            @RequestParam("property") String[] properties,
            @ApiParam(value = "Comparison operator to apply" ,required=true )
            @RequestParam("comp") String[] comps,
            @ApiParam(value = "Value of the filter property" ,required=true )
            @RequestParam("value") String[] values,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure",required=true)
            @RequestParam("access_token") String accessToken);

    @ApiOperation(value = "Update entity properties", nickname = "updateEntity", notes = "",
            response = Entity.class, tags={ "Discovery Service", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Entity successfully updated"),
            @ApiResponse(code = 400, message = "Provided data is invalid"),
            @ApiResponse(code = 401, message = "Unauthorized access"),
            @ApiResponse(code = 404, message = "Resource not found") })
    @PutMapping(value = "/discovery/{entityId}",
            consumes = { "application/json" })
    ResponseEntity<Object> update(
            @ApiParam(value = "Entity to send on the request" ,required=true )
            @Valid @RequestBody Entity body,
            @ApiParam(value = "Id of the entity specified",required=true)
            @PathVariable("entityId") String entityId,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure",required=true)
            @RequestParam("access_token") String accessToken);

    @ApiOperation(value = "Delete an entity by id", nickname = "deleteEntity", notes = "", tags={ "Discovery Service", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Entity successfully deleted"),
            @ApiResponse(code = 400, message = "Provided data is invalid"),
            @ApiResponse(code = 401, message = "Unauthorized access"),
            @ApiResponse(code = 404, message = "Resource not found") })
    @DeleteMapping(value = "/discovery/{entityId}")
    ResponseEntity<Object> delete(
            @ApiParam(value = "Id of the entity specified",required=true)
            @PathVariable("entityId") String entityId,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure",required=true)
            @RequestParam("access_token") String accessToken);
}
