/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.api;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.inov.eunomia.model.communication.Message;
import pt.inov.eunomia.model.communication.Service;

import javax.validation.Valid;

@Api(value = "communication", description = "The Eunomia Communication API. " +
        "Provides methods allowing nodes to communicate with each other.")
public interface CommunicationAPI {

    @ApiOperation(value = "Get all messages", nickname = "getInbox", notes = "",
            tags={ "Communication Service", },
            response = Message.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Inbox successfully retrieved"),
            @ApiResponse(code = 400, message = "Provided data is invalid"),
            @ApiResponse(code = 401, message = "Unauthorized access to perform the operation") })
    @GetMapping(value = "/communication")
    ResponseEntity<Object> getInbox(
            @ApiParam(value = "Id of the service inbox to be retrieved", required = true)
            @RequestParam("service_id") String serviceId,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure", required = true)
            @RequestParam("access_token") String accessToken);

    @ApiOperation(value = "Send Message to other service", nickname = "sendMessage", notes = "",
            response = Message.class, tags={ "Communication Service", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Message sent successfully"),
            @ApiResponse(code = 400, message = "Provided data is invalid"),
            @ApiResponse(code = 401, message = "Unauthorized access"),
            @ApiResponse(code = 404, message = "Resource not found") })
    @PostMapping(value = "/communication/send",
            consumes = { "application/json" })
    ResponseEntity<Object> sendMessage(
            @ApiParam(value = "Message to be sent.", required = true )
            @Valid @RequestBody Message message,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure", required = true)
            @RequestParam("access_token") String accessToken);

    @ApiOperation(value = "Receive message from other service", nickname = "receiveMessage", notes = "",
            tags={ "Communication Service", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Message received successfully"),
            @ApiResponse(code = 400, message = "Provided data is invalid"),
            @ApiResponse(code = 401, message = "Unauthorized access"),
            @ApiResponse(code = 404, message = "Resource not found") })
    @PostMapping(value = "/communication/receive",
            consumes = { "application/json" })
    ResponseEntity<Object> receiveMessage(
            @ApiParam(value = "Message to be received from other service.", required = true )
            @Valid @RequestBody Message message);

    @ApiOperation(value = "Bind service with a callback", nickname = "bindService", notes = "",
            response = Service.class, tags={ "Communication Service", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Service bound successfully"),
            @ApiResponse(code = 400, message = "Provided data is invalid"),
            @ApiResponse(code = 401, message = "Unauthorized access"),
            @ApiResponse(code = 404, message = "Resource not found") })
    @PostMapping(value = "/communication/bind",
            consumes = { "application/json" })
    ResponseEntity<Object> bind(
            @ApiParam(value = "Service to be registered.", required = true)
            @RequestBody Service service,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure", required = true)
            @RequestParam("access_token") String accessToken);

    @ApiOperation(value = "Unbind service from the callback", nickname = "unbindService", notes = "", tags={ "Communication Service", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Service unbound successfully"),
            @ApiResponse(code = 400, message = "Provided data is invalid"),
            @ApiResponse(code = 401, message = "Unauthorized access"),
            @ApiResponse(code = 404, message = "Resource not found") })
    @DeleteMapping(value = "/communication/unbind")
    ResponseEntity<Object> unbind(
            @ApiParam(value = "Id of the service to be unbound", required = true)
            @RequestParam("service_id") String serviceId,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure", required = true)
            @RequestParam("access_token") String accessToken);
}
