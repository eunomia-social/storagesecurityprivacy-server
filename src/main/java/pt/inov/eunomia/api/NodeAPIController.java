/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pt.inov.eunomia.model.node.NodeInformation;
import pt.inov.eunomia.services.node.NodeService;

@RestController
public class NodeAPIController implements NodeAPI{

    private static final Logger log = LoggerFactory.getLogger(NodeAPIController.class);

    @Autowired
    private NodeService nodeInformationService;

    @Override
    public ResponseEntity<NodeInformation> getNodeInformation() {

        log.info("getNodeInformation()");

        NodeInformation nodeInformation = nodeInformationService.getNodeInformation();

        log.info("getNodeInformation() returning: Node Information ");

        return new ResponseEntity<NodeInformation>(nodeInformation , new HttpHeaders(), HttpStatus.OK);
    }
}
