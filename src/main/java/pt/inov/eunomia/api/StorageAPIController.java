/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.ApiParam;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pt.inov.eunomia.api.responses.ApiError;
import pt.inov.eunomia.model.storage.StorageObject;
import pt.inov.eunomia.api.responses.ApiResponseMessage;
import pt.inov.eunomia.services.ledger.LedgerIntegrityService;
import pt.inov.eunomia.services.storage.StorageService;
import pt.inov.eunomia.util.Util;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import pt.inov.eunomia.model.LedgerEntryEntity;

@RestController
public class StorageAPIController implements StorageAPI {

    private static final Logger log = LoggerFactory.getLogger(StorageAPIController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private StorageService storageService;

    @Autowired
    private LedgerIntegrityService ledgerIntegrityService;

    @org.springframework.beans.factory.annotation.Autowired
    public StorageAPIController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Object> create(
            @ApiParam(value = "Object object to send on the request" ,required=true )
            @Valid @RequestBody StorageObject body,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's AAA API",required=true)
            @RequestParam("access_token") String accessToken) {

        log.info("create()");

        StorageObject storageObject = this.storageService.createObject(body);

        if (storageObject == null) {
            ApiError error = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Unable to create object");
            log.error("create() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }

        ObjectNode rootNode = objectMapper.createObjectNode();
        ObjectNode objectCreated = this.objectMapper.valueToTree(storageObject);
        rootNode.put("object", objectCreated);

        ApiResponseMessage responseMessage = new ApiResponseMessage(
                HttpStatus.OK,
                "Object successfully created",
                rootNode);

        log.info("create() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(responseMessage, new HttpHeaders(), responseMessage.getStatus());
    }

    public ResponseEntity<Object> get(
            @ApiParam(value = "Id of the object specified",required=true)
            @PathVariable("objectId") String objectId,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's AAA API",required=true)
            @RequestParam("access_token") String accessToken) {

        log.info("get() with id: " + objectId);

        StorageObject storageObject = this.storageService.getObject(objectId);
        if (storageObject == null) {
            ApiError error = new ApiError(HttpStatus.NOT_FOUND, "Couldn't find an object with " +
                    "the id provided");
            log.error("get() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }

        storageObject.setValidated(false);
        this.ledgerIntegrityService.verifySignature(storageObject);
        ObjectNode rootNode = objectMapper.createObjectNode();
        ObjectNode objectFetched = this.objectMapper.valueToTree(storageObject);
        rootNode.put("object", objectFetched);

        ApiResponseMessage responseMessage = new ApiResponseMessage(
                HttpStatus.OK,
                "Object successfully retrieved",
                rootNode);

        log.info("get() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(responseMessage, new HttpHeaders(), responseMessage.getStatus());
    }

    public ResponseEntity<Object> update(
            @ApiParam(value = "Object object to send on the request" ,required=true )
            @Valid @RequestBody StorageObject body,
            @ApiParam(value = "Id of the object specified",required=true)
            @PathVariable("objectId") String objectId,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's AAA API",required=true)
            @RequestParam("access_token") String accessToken) {

        log.info("update() with id: " + objectId);

        StorageObject storageObject = this.storageService.getObject(objectId);
        if (storageObject == null) {
            ApiError error = new ApiError(HttpStatus.NOT_FOUND, "Couldn't find an object with " +
                    "the id provided");
            log.error("update() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }

        StorageObject newStorageObject = this.storageService.updateObject(body, storageObject);
        if (newStorageObject == null) {
            ApiError error = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Unable to create a Ledger Entry");
            log.error("update() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }

        ObjectNode objectUpdated = this.objectMapper.valueToTree(newStorageObject);

        ObjectNode rootNode = objectMapper.createObjectNode();
        rootNode.put("object", objectUpdated);

        ApiResponseMessage responseMessage = new ApiResponseMessage(
                HttpStatus.OK,
                "Object successfully updated",
                rootNode);

        log.info("update() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(responseMessage, new HttpHeaders(), responseMessage.getStatus());
    }

    public ResponseEntity<Object> delete(
            @ApiParam(value = "Id of the post specified",required=true)
            @PathVariable("objectId") String objectId,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's AAA API",required=true)
            @RequestParam("access_token") String accessToken) {

        log.info("delete() with id: " + objectId);

        this.storageService.deleteObject(objectId);

        ApiResponseMessage responseMessage = new ApiResponseMessage(
                HttpStatus.OK,
                "Object successfully deleted");

        log.info("delete() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(responseMessage, new HttpHeaders(), responseMessage.getStatus());
    }

    @Override
    public ResponseEntity<Object> list(
            @ApiParam(value = "Name of the property to filter track entries by",required=true)
            @RequestParam("property") String[] properties,
            @ApiParam(value = "comparison operator to apply",required=true)
            @RequestParam("comp") String[] comps,
            @ApiParam(value = "Value of the property",required=true)
            @RequestParam("value") String[] values,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure",required=true)
            @RequestParam("access_token") String accessToken) {

        log.info("list()");

        if (properties.length != values.length || comps.length != values.length) {
            ApiError error = new ApiError(HttpStatus.BAD_REQUEST,
                    "The properties/comparisons/values don't match");
            log.error("list() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }
        ArrayNode objects = this.storageService.list(properties, comps, values);

        if (objects == null) {
            ApiError error = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to list storage objects");
            log.error("list() Error: " + error.getMessage());
            return new ResponseEntity<Object>(error, new HttpHeaders(), error.getStatus());
        }
        
        String message="";
        ArrayNode validateObjects=this.objectMapper.createArrayNode();
        if (objects.size() > 0) {
            ArrayList<LedgerEntryEntity> objsToValidate = new ArrayList<>();
            for (int i = 0; i < objects.size(); i++) {
                JsonNode node = objects.get(i);
                StorageObject storageObjectUpdated = null;
                try {
                    storageObjectUpdated = this.objectMapper.treeToValue(node, StorageObject.class);
                } catch (JsonProcessingException e) {
                    log.error("updateObject() Error: ", e);
                    return null;
                }
                objsToValidate.add(storageObjectUpdated);
            }

            ledgerIntegrityService.verifySignatures(objsToValidate);

            for (LedgerEntryEntity ledgerEntryEntity : objsToValidate) {
                StorageObject storageObjectUpdated = (StorageObject) ledgerEntryEntity;
                validateObjects.add(this.objectMapper.valueToTree(storageObjectUpdated));
            }
            message = "Objects successfully listed";
        }else {
            message = "No objects found";
        }

        ApiResponseMessage responseMessage = new ApiResponseMessage(
                HttpStatus.OK,
                message,
                validateObjects);

        log.info("list() returning: " + responseMessage.getMessage());

        return new ResponseEntity<Object>(responseMessage, new HttpHeaders(), responseMessage.getStatus());
    }
}
