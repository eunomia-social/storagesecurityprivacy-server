/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.api;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.inov.eunomia.model.tracking.TrackEntry;

import javax.validation.Valid;

@Api(value = "track", description = "The Tracking Service API")
public interface TrackingAPI {

    @ApiOperation(value = "Create entry in the Ledger Service",
            nickname = "createEntry", notes = "", response = Object.class, tags={ "Tracking Service" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Entry successfully retrieved", response = Object.class),
            @ApiResponse(code = 400, message = "Invalid Operation", response = TrackEntry.class),
            @ApiResponse(code = 401, message = "Unauthorized access to perform the operation") })
    @PostMapping(value = "/entry",
            produces = { "application/json" })
    ResponseEntity<Object> create(
            @ApiParam(value = "Entry details" ,required=true )
            @Valid @RequestBody TrackEntry trackEntry,
            @ApiParam(value = "Eunomia Access Token", required = true)
            @RequestParam("access_token") String accessToken);

    @ApiOperation(value = "Get entry from Ledger Service",
            nickname = "getEntry", notes = "", response = Object.class, tags={ "Tracking Service" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Entry successfully retrieved", response = Object.class),
            @ApiResponse(code = 400, message = "Invalid Operation", response = Object.class),
            @ApiResponse(code = 401, message = "Unauthorized access to perform the operation") })
    @GetMapping(value = "/entry/{entry_id}",
            produces = { "application/json" })
    ResponseEntity<Object> get(
            @ApiParam(value = "Eunomia Access Token", required = true)
            @RequestParam("access_token") String accessToken,
            @ApiParam(value = "Entry ID", required = true)
            @PathVariable("entry_id") String entryId);

    @ApiOperation(value = "List track entries by a given property", nickname = "listTrackEntries", notes = "", tags={ "Tracking Service", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Track Entries successfully listed"),
            @ApiResponse(code = 400, message = "Provided data is invalid"),
            @ApiResponse(code = 401, message = "Unauthorized access"),
            @ApiResponse(code = 404, message = "Resources not found") })
    @GetMapping(value = "/entry")
    ResponseEntity<Object> list(
            @ApiParam(value = "Name of the property to filter track entries by",required=true)
            @RequestParam("property") String[] properties,
            @ApiParam(value = "Comparison operator to apply",required=true)
            @RequestParam("comp") String[] comps,
            @ApiParam(value = "Value of the property",required=true)
            @RequestParam("value") String[] values,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure",required=true)
            @RequestParam("access_token") String accessToken);
}
