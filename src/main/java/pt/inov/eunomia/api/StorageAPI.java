/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.api;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.inov.eunomia.model.storage.StorageObject;

import javax.validation.Valid;

@Api(value = "objects", description = "The Eunomia objects API")
public interface StorageAPI {

    @ApiOperation(value = "Create an object", nickname = "createObject", notes = "",
            tags={ "Storage Service", },
            response = StorageObject.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Object successfully created"),
            @ApiResponse(code = 400, message = "Provided data is invalid"),
            @ApiResponse(code = 401, message = "Unauthorized access to perform the operation") })
    @PostMapping(value = "/objects",
            consumes = { "application/json" })
    ResponseEntity<Object> create(
            @ApiParam(value = "Object to send on the request" ,required=true )
            @Valid @RequestBody StorageObject body,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure",required=true)
            @RequestParam("access_token") String accessToken);

    @ApiOperation(value = "Get an object by id", nickname = "getObject", notes = "", response = StorageObject.class,
            tags={ "Storage Service", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Object successfully retrieved", response = StorageObject.class),
            @ApiResponse(code = 400, message = "Invalid operation"),
            @ApiResponse(code = 401, message = "Unauthorized access"),
            @ApiResponse(code = 404, message = "Resource not found") })
    @GetMapping(value = "/objects/{objectId}",
            produces = { "application/json" })
    ResponseEntity<Object> get(
            @ApiParam(value = "Id of the object specified",required=true)
            @PathVariable("objectId") String objectId,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure",required=true)
            @RequestParam("access_token") String accessToken);

    @ApiOperation(value = "Update object properties", nickname = "updateObject", notes = "",
            response = StorageObject.class, tags={ "Storage Service", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Object successfully updated"),
            @ApiResponse(code = 400, message = "Provided data is invalid"),
            @ApiResponse(code = 401, message = "Unauthorized access"),
            @ApiResponse(code = 404, message = "Resource not found") })
    @PutMapping(value = "/objects/{objectId}",
            consumes = { "application/json" })
    ResponseEntity<Object> update(
            @ApiParam(value = "Object to send on the request" ,required=true )
            @Valid @RequestBody StorageObject body,
            @ApiParam(value = "Id of the object specified",required=true)
            @PathVariable("objectId") String objectId,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure",required=true)
            @RequestParam("access_token") String accessToken);

    @ApiOperation(value = "Delete an object by id", nickname = "deleteObject", notes = "", tags={ "Storage Service", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Object successfully deleted"),
            @ApiResponse(code = 400, message = "Provided data is invalid"),
            @ApiResponse(code = 401, message = "Unauthorized access"),
            @ApiResponse(code = 404, message = "Resource not found") })
    @DeleteMapping(value = "/objects/{objectId}")
    ResponseEntity<Object> delete(
            @ApiParam(value = "Id of the object specified",required=true)
            @PathVariable("objectId") String objectId,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure",required=true)
            @RequestParam("access_token") String accessToken);

    @ApiOperation(value = "List all objects by a given property", nickname = "listObjects", notes = "", tags={ "Storage Service", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Objects successfully listed"),
            @ApiResponse(code = 400, message = "Provided data is invalid"),
            @ApiResponse(code = 401, message = "Unauthorized access"),
            @ApiResponse(code = 404, message = "Resources not found") })
    @GetMapping(value = "/objects")
    ResponseEntity<Object> list(
            @ApiParam(value = "Name of the property to filter track entries by",required=true)
            @RequestParam("property") String[] properties,
            @ApiParam(value = "Comparison operator to apply",required=true)
            @RequestParam("comp") String[] comps,
            @ApiParam(value = "Value of the property",required=true)
            @RequestParam("value") String[] values,
            @ApiParam(value = "Eunomia Access Token retrieved by Eunomia's infrastructure",required=true)
            @RequestParam("access_token") String accessToken);
}
