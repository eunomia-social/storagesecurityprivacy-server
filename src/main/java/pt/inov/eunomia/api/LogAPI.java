/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * This API is used to allow logging inside the instance.
 * @author lmss
 */
@Api(value = "log", description = "The Eunomia Node Log API. This API will allow to log information locally in this node.")
public interface LogAPI {

    @ApiOperation(value = "Create a log line entry in this Storage Server internal log.",
            nickname = "createLog", notes = "", response = Object.class, tags = {"Log Service"})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Line successfully created"),
        @ApiResponse(code = 400, message = "Invalid Operation"),
        @ApiResponse(code = 401, message = "Unauthorized access to perform the operation")})
    @PostMapping(value = "/log",
            produces = {"application/json"},
            consumes = "text/plain")
    ResponseEntity<Object> log(
            @ApiParam(value = "Eunomia Access Token", required = true)
            @RequestParam("access_token") String accessToken, 
            @RequestParam(value = "Log Category", required = true) String category,
            //@ApiParam(value ="Log Line") 
            @RequestBody String line);
}
