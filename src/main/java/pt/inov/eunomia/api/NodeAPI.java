/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import pt.inov.eunomia.model.node.NodeInformation;

@Api(value = "node", description = "The Eunomia Node API. This API will give generic information about the node.")
public interface NodeAPI {
    @ApiOperation(value = "Get node generic information.", nickname = "nodeInformationGet", notes = "", response = NodeInformation.class, tags = {"Node Information Service"})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Information successfully retrieved.")})
    @GetMapping(value = "/node/info", produces = {"application/json"})
    ResponseEntity<NodeInformation> getNodeInformation();
}
