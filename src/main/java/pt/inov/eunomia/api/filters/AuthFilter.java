/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.api.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import pt.inov.eunomia.api.responses.ApiError;
import pt.inov.eunomia.db.orbitdb.AuthOrbitDbClient;
import pt.inov.eunomia.conf.OrbitDbConfig;
import pt.inov.eunomia.services.auth.AuthService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Order(2)
public class AuthFilter implements javax.servlet.Filter {

    @Autowired
    private OrbitDbConfig orbitDbConfig;

    private AuthOrbitDbClient authDatabase;

    @Autowired
    private AuthService authService;

    private static final Logger log = LoggerFactory.getLogger(AuthFilter.class);

    private List<String> excludedPaths = new ArrayList<>();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("init() Invoked.");       
        this.authDatabase = new AuthOrbitDbClient( this.orbitDbConfig.getUrl(), this.orbitDbConfig.getAuthDatabaseCid(), this.orbitDbConfig.getTimeoutMillis());
        this.excludedPaths = Arrays.asList("/eunomia/token", "/docs", "/swagger-ui.html","/webjars/springfox-swagger-ui", "/swagger-resources", "/node", "/entry", "/communication/receive");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        HttpServletResponse res = (HttpServletResponse) response;
        res.addHeader("Access-Control-Allow-Origin", "*");
        res.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        res.addHeader("Access-Control-Allow-Headers", "Content-Type");

        log.info("doFilter() Authentication filter");
        long start = System.currentTimeMillis();

        String path = ((HttpServletRequest) request).getServletPath();
        
        ApiError error = new ApiError(HttpStatus.UNAUTHORIZED, "Invalid Token");

        if (!this.checkPathContained(path)) {
            String eunomiaToken = request.getParameter("access_token");

            if (!this.authService.tokenIsValid(eunomiaToken,null)) {
                log.error("doFilter() Error: The access token may have expired or been revoked.");
                error.setErrors(Arrays.asList("The access token may have expired or been revoked."));
                byte[] errorBytes = restResponseBytes(error);
                ((HttpServletResponse) response).setHeader("Content-Type", "application/json");
                ((HttpServletResponse) response).setStatus(HttpStatus.UNAUTHORIZED.value());
                response.getOutputStream().write(errorBytes);
                return;
            }
            log.info("doFilter() Authentication with token is valid");
        }

        try {
            chain.doFilter(request, response);
        } catch (Exception e) {
            log.error("doFilter() Error processing request: " , e);
            throw e;
        } finally {
            long elapsedTime = System.currentTimeMillis() - start;
            log.info("doFilter() response took: " + elapsedTime + " ms");
        }
    }

    @Override
    public void destroy() {
    }

    private byte[] restResponseBytes(ApiError error) throws IOException {
        String serialized = new ObjectMapper().writeValueAsString(error);
        return serialized.getBytes();
    }

    private boolean checkPathContained(String path) {

        log.debug("checkPathContained() path: '" + path + "'");

        for (String excludedPath : this.excludedPaths) {
            if (path.contains(excludedPath)) {
                log.debug("checkPathContained() path is contained");
                return true;
            }
        }

        log.debug("checkPathContained() path is not contained");

        return false;
    }
}
