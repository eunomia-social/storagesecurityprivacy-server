/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.api.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Arrays;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.core.annotation.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import pt.inov.eunomia.api.responses.ApiError;

/**
 * This class blocks incoming HTTP thread requests unless it's enabled. Please
 * note that the threads will be indeed blocked, unless it's enabled again,
 * where the pending threads will be unlocked.
 *
 * @author lmss
 */
@Component
@Order(1)
public class HTTPRequestEnablerFilter implements javax.servlet.Filter {

    private static boolean enabled = false;
    private final static Object lock = new Object();
    private static final Logger logger = LoggerFactory.getLogger(HTTPRequestEnablerFilter.class);

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        enabled = false;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String url = ((HttpServletRequest) request).getRequestURL().toString();
        if (!isEnabled()) {
            synchronized (lock) {
                try {
                    logger.info("doFilter() Locking request due to server not being ready: " + url);
                    lock.wait();
                    logger.info("doFilter() Unlocking pending request since server is processing: " + url);
                    chain.doFilter(request, response);
                } catch (InterruptedException ex) {
                    logger.error("doFilter() Isn't ready but request failed to be locked: " + url);
                    //in this case we don't want to proceed so return a 503 Service unavailable.
                    logger.error("doFilter() Error: Unexpected Interrupt. Refusing to continue processing with 503!");
                    ApiError error = new ApiError(HttpStatus.SERVICE_UNAVAILABLE, "Server is busy and not currently processing.");
                    error.setErrors(Arrays.asList("Server is busy and actively refusing to process requests."));
                    byte[] errorBytes = restResponseBytes(error);
                    ((HttpServletResponse) response).setHeader("Content-Type", "application/json");
                    ((HttpServletResponse) response).setStatus(HttpStatus.SERVICE_UNAVAILABLE.value());
                    response.getOutputStream().write(errorBytes);
                }
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        synchronized (lock) {
            return enabled;
        }
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        synchronized (lock) {
            if (enabled) {
                logger.info("setEnable() Reenabling HTTP processing!");
                lock.notifyAll();

            } else {
                logger.info("setEnabled() Disabling HTTP processing, requests will be blocked!");
            }
            HTTPRequestEnablerFilter.enabled = enabled;
        }

    }

    private byte[] restResponseBytes(ApiError error) throws IOException {
        String serialized = new ObjectMapper().writeValueAsString(error);
        return serialized.getBytes();
    }
}
