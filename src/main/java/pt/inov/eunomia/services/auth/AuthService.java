/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.services.auth;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import pt.inov.eunomia.conf.LedgerConfig;
import pt.inov.eunomia.conf.OrbitDbConfig;
import pt.inov.eunomia.conf.SNConfig;
import pt.inov.eunomia.db.ledger.LedgerDbClient;
import pt.inov.eunomia.model.ledger.LedgerEntry;
import pt.inov.eunomia.db.orbitdb.AuthOrbitDbClient;
import pt.inov.eunomia.db.orbitdb.StorageOrbitDbClient;
import pt.inov.eunomia.model.auth.AuthObject;
import pt.inov.eunomia.model.storage.StorageObject;

import javax.annotation.PostConstruct;
import java.util.Calendar;
import java.util.Date;
import pt.inov.eunomia.services.ledger.LedgerIntegrityService;
import pt.inov.eunomia.util.RandomUtil;
import pt.inov.eunomia.util.Util;

@Service
public class AuthService {

    private static final Logger log = LoggerFactory.getLogger(AuthService.class);

    @Autowired
    private OrbitDbConfig orbitDbConfig;

    @Autowired
    private SNConfig snConfig;

    private AuthOrbitDbClient authDatabase;

    private StorageOrbitDbClient objectsDatabase;

    @Autowired
    private LedgerIntegrityService ledgerIntegrityService;

    private ObjectMapper objectMapper;

    @PostConstruct
    public void instantiateDatabases() {
        log.info("instantiateDatabases() Connection to Auth database with the address");
        try {
            this.authDatabase = new AuthOrbitDbClient(
                    this.orbitDbConfig.getUrl(),
                    this.orbitDbConfig.getAuthDatabaseCid(), this.orbitDbConfig.getTimeoutMillis());
            log.info("instantiateDatabases() Connection to Model database with the address");
            this.objectsDatabase = new StorageOrbitDbClient(this.orbitDbConfig.getUrl(), this.orbitDbConfig.getModelDatabaseCid(), this.orbitDbConfig.getTimeoutMillis());
        } catch (Exception e) {
            log.error("instantiateDatabases() Error: ", e);
        }
        this.objectMapper = new ObjectMapper();
    }

    public ObjectNode verifyAccountWithDsn(String accessToken, String authProviderUri) {

        log.info("verifyAccountWithDsn()");

        RestTemplate restTemplate = Util.getRestTemplate(this.snConfig.getTimeoutMillis());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + accessToken);
        HttpEntity<String> authRequest = new HttpEntity<String>(headers);
        ResponseEntity<JsonNode> authResponse = null;
        try {
            authResponse = restTemplate.exchange("https://" + authProviderUri
                    + this.snConfig.getMastodon() + "?access_token=" + accessToken,
                    HttpMethod.GET, authRequest, JsonNode.class);
        } catch (RestClientException e) {
            log.error("verifyAccountWithDsn() Error: ", e);
            return null;
        }

        if (authResponse.getBody().has("error")) {
            log.error("verifyAccountWithDsn() Error: Unauthorized Access with mastodon token " + accessToken
                    + " in " + authProviderUri + " server");
            return null;
        }

        log.info("verifyAccountWithDsn() returning user data");

        return (ObjectNode) authResponse.getBody();
    }

    public ObjectNode createAccount(ObjectNode userData) {
        ObjectNode user = null;
        log.info("createAccount() With userData:"+userData+ " using sn.id="+snConfig.getId());
        try {
            String[] props = {"properties.id"};
            String[] comps = {"eq"};
            String[] values = {userData.get("id").asText() + "@" + snConfig.getId()};

            ArrayNode arrayNode = this.objectsDatabase.getItemsByProperty(props, comps, values);
            if (arrayNode != null) {
                user = (ObjectNode) arrayNode.get(0);
                if(user!=null){
                    StorageObject storageObject = this.objectMapper.treeToValue(user, StorageObject.class);
                    ledgerIntegrityService.verifySignature(storageObject);
                    user=this.objectMapper.valueToTree(storageObject);
                }
            }

            if (user == null) {
                StorageObject storageObject = new StorageObject();
                storageObject.setId(this.objectsDatabase.generateId());
                // TODO: Change Ledger Entry Id to a UUID
                storageObject.setLedgerEntryId(RandomUtil.generateRandomInt(1000000));
                storageObject.setType(StorageObject.TYPE);
                userData.put("id", userData.get("id").asText() + "@" + snConfig.getId());
                //userData.remove("id");
                storageObject.setProperties(userData);

                LedgerEntry ledgerEntry = storageObject.toLedgerEntry();
                boolean result = ledgerIntegrityService.createEntry(ledgerEntry);
                if (!result) {
                    log.info("createAccount() Error: Unable to create a Ledger Entry");
                    return null;
                }
                
                user = this.objectMapper.valueToTree(storageObject);
                ObjectNode objectCreated = this.objectsDatabase.createItem(user);

                if (objectCreated == null) {
                    log.error("createAccount() Error: Unable to create account");
                    return null;
                }
                //assume true even being the creation.
                storageObject.setValidated(true);
                user = this.objectMapper.valueToTree(storageObject);
            }

            log.info("createAccount() returning the User object node");
        } catch (Exception e) {
            log.error("createAccount() ERROR:", e);
        }
        return user;
    }

    /**
     * Generate a given token for a given userId
     *
     * @param userId
     * @return The generated token.
     */
    public String generateToken(String userId) {

        log.info("generateToken() With userId:" + userId);

        AuthObject authObject = new AuthObject();
        authObject.setToken(this.authDatabase.generateToken());
        // TODO: Change Ledger Entry Id to a UUID
        authObject.setLedgerEntryId(RandomUtil.generateRandomInt(1000000));
        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        calendar.add(Calendar.HOUR_OF_DAY, 1);
        Date expireDate = calendar.getTime();
        authObject.setCreatedAt(now);
        authObject.setExpiredAt(expireDate);
        authObject.setUserId(userId);

        LedgerEntry ledgerEntry = authObject.toLedgerEntry();
        boolean result = ledgerIntegrityService.createEntry(ledgerEntry);
        if (!result) {
            log.info("generateToken() Error: Unable to create a Ledger Entry");
            return null;
        }

        ObjectNode objectToCreate = this.objectMapper.valueToTree(authObject);

        ObjectNode objectCreated = this.authDatabase.createItem(objectToCreate);

        if (objectCreated == null) {
            log.error("generateToken() Error: Unable to create Auth Object");
            return null;
        }

        log.info("generateToken() returning the EUNOMIA Token");
        return authObject.getToken();
    }

    /**
     * It checks if given token is valid
     *
     * @param token The token to validate.
     * @para userId The userId if not null it will also check if this user
     * belongs to the provided token.
     * @return True if OK false otherwise.
     */
    public boolean tokenIsValid(String token, String userId) {

        log.info("tokenIsValid() Invoked to check token:" + token + " and userId:" + userId);

        if (token == null) {
            log.error("tokenIsValid() Error: The eunomia access token provided is invalid");
            return false;
        }

        if (token.length() != AuthOrbitDbClient.TOKEN_SIZE) {
            log.error("tokenIsValid() Error: The eunomia access token provided is invalid");
            return false;
        }

        ObjectNode authObjectFetched = this.authDatabase.getItem(token);

        if (authObjectFetched == null) {
            log.error("tokenIsValid() Error: Unable to fetch Auth Object");
            return false;
        }

        AuthObject authObject = null;
        try {
            authObject = this.objectMapper.treeToValue(authObjectFetched, AuthObject.class);
        } catch (JsonProcessingException e) {
            log.error("tokenIsValid() Error: " + e);
            return false;
        }

        if (authObject == null) {
            log.error("tokenIsValid() Error: The eunomia access token provided is invalid");
            return false;
        }

        if (authObject.checkTokenExpired()) {
            log.error("tokenIsValid() Error: The eunomia access token provided has expired");
            return false;
        }

        if (authObject.isRevoked()) {
            log.error("tokenIsValid() Error: The eunomia access token provided has been revoked");
            return false;
        }

        if (userId != null) {
            if (!userId.equals(authObject.getUserId())) {
                log.error("tokenIsValid() Error: The eunomia access token provided is ok but the given user id :" + userId + " doesn't match with the recorded one: " + authObject.getUserId());
                return false;
            }
        }

        log.info("tokenIsValid() returning: Token is valid");

        return true;
    }

    public void revokeToken(String token) {

        log.info("revokeToken()");

        ObjectNode objectFetched = this.authDatabase.getItem(token);

        if (objectFetched == null) {
            log.error("revokeToken() Error: Unable to get Auth Object");
            return;
        }

        AuthObject authObject = null;
        try {
            authObject = this.objectMapper.treeToValue(objectFetched, AuthObject.class);
        } catch (JsonProcessingException e) {
            log.error("revokeToken() Error: ", e);
            return;
        }
        authObject.setRevoked(true);
        this.authDatabase.deleteItem(authObject.getToken());
        ObjectNode objectToCreate = this.objectMapper.valueToTree(authObject);
        this.authDatabase.createItem(objectToCreate);

        log.info("revokeToken() token revoked");
    }
}
