/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.services.communication;

import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;
import pt.inov.eunomia.conf.CommunicationConfig;
import pt.inov.eunomia.db.communication.Inbox;
import pt.inov.eunomia.db.communication.Outbox;
import pt.inov.eunomia.db.communication.ServicePersistence;
import pt.inov.eunomia.model.communication.Message;
import pt.inov.eunomia.model.communication.Receiver;
import pt.inov.eunomia.model.communication.Service;
import pt.inov.eunomia.services.discovery.DiscoveryService;
import pt.inov.eunomia.util.Util;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

@org.springframework.stereotype.Service
public class CommunicationService {

    private static final Logger log = LoggerFactory.getLogger(CommunicationService.class);

    @Autowired
    private CommunicationConfig communicationConfig;

    @Autowired
    private ServicePersistence servicePersistence;

    @Autowired
    private Inbox inbox;

    @Autowired
    private Outbox outbox;

    @Autowired
    private DiscoveryService discoveryService;

    private boolean readyFlag = false;

    public void initialize() {
        this.readyFlag = true;
        log.info("initialize()");
    }

    public ArrayList<Message> getInbox(String serviceId) {
        log.info("getInbox() init");

        ArrayList<Message> messages = this.inbox.getServiceMessages(serviceId);

        log.info("getInbox() finished");

        return messages;
    }

    public boolean sendMessage(Message message) {
        log.info("sendMessage()");

        boolean result = this.outbox.saveMessage(message);

        log.info("sendMessage() finished");

        return result;
    }

    public boolean receiveMessage(Message message) {
        log.info("receiveMessage() init");

        boolean result = this.inbox.saveMessage(message);

        log.info("receiveMessage() finished");

        return result;
    }

    public Service bind(Service service) {
        log.info("bind() init");

        Service serviceFetched = this.servicePersistence.getService(service.getId());
        if (serviceFetched != null) {
            this.servicePersistence.deleteService(service.getId());
        }

        Service serviceCreated = this.servicePersistence.createService(service);

        if (serviceCreated == null) {
            log.error("Unable to create service");
            return null;
        }

        log.info("bind() finished");

        return serviceCreated;
    }

    public void unbind(String serviceId) {
        log.info("unbind() init");

        this.servicePersistence.deleteService(serviceId);

        log.info("unbind() finished");
    }

    @Scheduled(fixedRateString = "${communication.delivery_interval_millis}")
    public void sendMessagesFromInbox() {
        if (! this.readyFlag) {
            return;
        }

        log.debug("sendMessagesFromInbox() init");
        Object[] messagesAndServices = this.inbox.getPendingMessagesWithServices();
        ArrayList<Message> messages = (ArrayList<Message>) messagesAndServices[0];
        ArrayList<Service> services = (ArrayList<Service>) messagesAndServices[1];
        ArrayList<Message> messagesSent = new ArrayList<>();

        if (services.isEmpty()) {
            log.debug("sendMessagesFromInbox() No services to receive the messages.");
            return;
        }

        if (messages.isEmpty()) {
            log.debug("sendMessagesFromInbox() No messages to be sent.");
            return;
        }

        for (Message message : messages) {
            Message messageSent = this.sendMessageToServices(message, services);
            if (messageSent != null) {
                // message sent
                messagesSent.add(message);
            }
        }

        this.inbox.setMessagesAsSent(messagesSent);
        log.debug("sendMessagesFromInbox() finished");
    }

    private Message sendMessageToServices(Message message, ArrayList<Service> services) {
        log.debug("sendMessageToService() init");
        int numberOfReceivers = message.getReceivers().size();
        ArrayList<Receiver> receivers = new ArrayList<>();
        for (Receiver receiver : message.getReceivers()) {
            String callback = this.getCallback(receiver, services);
            if (callback != null) {
                try {
                    log.debug("sendMessageToServices() service: " + receiver.getServiceId() + " callback: " + callback);
                    URI uri = new URI(callback);
                    RestTemplate restTemplate = Util.getRestTemplate(this.communicationConfig.getTimeoutMillis());

                    HttpEntity<Message> request = new HttpEntity<>(message);

                    ResponseEntity<JsonNode> response = restTemplate.exchange(uri,
                            HttpMethod.POST, request, JsonNode.class);

                    log.debug("sendMessageToServices() response: " + response.toString());

                    if (response.getStatusCode() == HttpStatus.OK) {
                        receivers.add(receiver);
                    }

                } catch (URISyntaxException e) {
                    log.error("Error: ", e);
                }
            }
        }

        if (numberOfReceivers == receivers.size()) {
            // all receivers got the message delivered
            log.debug("sendMessageToService() All message sent");
            return message;
        }

        // remove receivers which returned 200 from db
        this.inbox.removeReceivers(message, receivers);
        log.debug("sendMessageToService() " + receivers.size() + " messages sent from " + numberOfReceivers);
        return null;
    }

    private String getCallback(Receiver receiver, ArrayList<Service> services) {
        log.debug("getCallback() init");
        for (Service service : services) {
            if (service.getId().equals(receiver.getServiceId())) {
                return service.getCallback();
            }
        }
        log.debug("getCallback() finished");
        return null;
    }

    @Scheduled(fixedRateString = "${communication.delivery_interval_millis}")
    public void sendMessagesFromOutbox() {
        if (! this.readyFlag) {
            return;
        }

        log.debug("sendMessagesFromOutbox() init");
        ArrayList<Message> messages = this.outbox.getPendingMessagesWithOneReceiver();
        ArrayList<Message> messagesSent = new ArrayList<>();

        if (messages.isEmpty()) {
            log.debug("sendMessagesFromOutbox() No messages to be sent.");
            return;
        }

        // TODO: get service auth token

        for (Message message : messages) {
            Message messageSent = this.sendMessageToNodes(message);
            if (messageSent != null) {
                // message sent
                messagesSent.add(message);
            }
        }

        this.outbox.setMessagesAsSent(messagesSent);
        log.debug("sendMessagesFromOutbox() finished");
    }

    private Message sendMessageToNodes(Message message) {
        log.debug("sendMessageToNodes() init");
        ArrayList<Receiver> receivers = new ArrayList<>();
        int numberOfReceivers = message.getReceivers().size();
        for (Receiver receiver : message.getReceivers()) {
            try {
                RestTemplate restTemplate = Util.getRestTemplate(this.communicationConfig.getTimeoutMillis());
                String nodeUrl = this.discoveryService.getNodeUrl(receiver.getNodeId());

                if (nodeUrl == null) {
                    log.debug("sendMessageToNodes() IP not specified for " + receiver.getNodeId());
                    continue;
                }

                String url = nodeUrl + "/api/communication/receive";
                URI uri = new URI(url);

                HttpEntity<Message> request = new HttpEntity<>(message);

                ResponseEntity<JsonNode> response = restTemplate.exchange(uri,
                        HttpMethod.POST, request, JsonNode.class);

                log.debug("sendMessageToNodes() response: " + response.toString());

                if (response.getStatusCode() == HttpStatus.OK) {
                    receivers.add(receiver);
                }

            } catch (URISyntaxException e) {
                log.error("Error: ", e);
            }
        }

        if (numberOfReceivers == receivers.size()) {
            // all nodes received the message
            log.debug("sendMessageToNodes() All messages sent");
            return message;
        }

        // remove receivers which returned 200 from db
        this.outbox.removeReceivers(message, receivers);
        log.debug("sendMessageToNodes() " + receivers.size() + " messages sent from " + numberOfReceivers);
        return null;
    }

    @Scheduled(fixedDelayString = "${communication.clear_sent_messages_interval_millis}")
    public void clearSentMessages() {
        if (! this.readyFlag) {
            return;
        }

        log.info("clearSentMessages() init");
        this.outbox.clearSentMessages();
        this.inbox.clearSentMessages();
        log.info("clearSentMessages() finished");
    }

    @Scheduled(fixedDelayString = "${communication.clear_old_messages_interval_millis}")
    public void clearOldMessages() {
        if (! this.readyFlag) {
            return;
        }

        log.info("clearOldMessages() init");
        this.inbox.clearOldMessages(this.communicationConfig.getTemporalCriteria());
        this.outbox.clearOldMessages(this.communicationConfig.getTemporalCriteria());
        log.info("clearOldMessages() finished");
    }
}
