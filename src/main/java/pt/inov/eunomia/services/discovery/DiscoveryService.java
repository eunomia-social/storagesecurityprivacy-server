/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.services.discovery;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.inov.eunomia.conf.OrbitDbConfig;
import pt.inov.eunomia.model.ledger.LedgerEntry;
import pt.inov.eunomia.db.orbitdb.DiscoveryOrbitDbClient;
import pt.inov.eunomia.model.discovery.Entity;
import pt.inov.eunomia.model.discovery.Property;
import pt.inov.eunomia.util.RandomUtil;

import javax.annotation.PostConstruct;
import pt.inov.eunomia.services.ledger.LedgerIntegrityService;

/**
 * This class aggregates all the business logic related to the discovery
 * service.
 *
 * @author lmss
 */
@Service
public class DiscoveryService {

    private static final Logger log = LoggerFactory.getLogger(DiscoveryService.class);

    @Autowired
    private OrbitDbConfig orbitDbConfig;

    private DiscoveryOrbitDbClient discoveryDatabase;

    @Autowired
    private LedgerIntegrityService ledgerIntegrityService;

    private ObjectMapper objectMapper;

    @PostConstruct
    private void instantiateConnections() {
        log.info("instantiateConnections() Connection to Discovery database with the address");
        try {
            this.discoveryDatabase = new DiscoveryOrbitDbClient(
                    this.orbitDbConfig.getUrl(),
                    this.orbitDbConfig.getDiscoveryDatabaseCid(),
                    this.orbitDbConfig.getTimeoutMillis()
            );
        } catch (Exception e) {
            log.error("instantiateConnections() Error: ", e);
        }
        this.objectMapper = new ObjectMapper();
    }

    /**
     * It registers a given Entity metadata in the discovery database.
     *
     * @return The registered entity or null in case of error.
     */
    public Entity registerEntity(Entity entity) {

        log.info("registerEntity()");

        // TODO: Change Ledger Entry Id to a UUID
        entity.setLedgerEntryId(RandomUtil.generateRandomInt(1000000));

        LedgerEntry ledgerEntry = entity.toLedgerEntry();

        log.info("Creating the Ledger Entry: " + ledgerEntry);
        boolean result = ledgerIntegrityService.createEntry(ledgerEntry);
        if (!result) {
            log.info("registerEntity() Error: Unable to create a Ledger Entry");
            return null;
        }

        ObjectNode objectToCreate = this.objectMapper.valueToTree(entity);
        ObjectNode objectCreated = this.discoveryDatabase.createItem(objectToCreate);

        if (objectCreated == null) {
            log.error("registerEntity() Error: Unable to create Entity");
            return null;
        }

        Entity entityCreated = null;
        try {
            entityCreated = this.objectMapper.treeToValue(objectCreated, Entity.class);
        } catch (JsonProcessingException e) {
            log.error("registerEntity() Error: ", e);
            return null;
        }

        log.info("registerEntity() returning the Entity registered");

        return entityCreated;
    }

    /**
     * Fetches a discovery entity by id.
     *
     * @return The entity fetched or null if it wasn't found.
     */
    public Entity get(String entityId) {

        log.info("get()");

        ObjectNode objectFetched = this.discoveryDatabase.getItem(entityId);

        if (objectFetched == null) {
            log.error("get() Error: Unable to fetch Entity");
            return null;
        }

        Entity entity = null;
        try {
            entity = this.objectMapper.treeToValue(objectFetched, Entity.class);
        } catch (Exception e) {
            log.error("get() Error: ", e);
        }

        log.info("get() returning the Entity fetched");

        return entity;
    }

    /**
     * Lists the entities by a provided property name and value.
     *
     * @return A json array of the entities.
     */
    public ArrayNode list(String[] properties, String[] comps, String[] values) {
        log.info("list()");

        ArrayNode entities = this.discoveryDatabase.getItemsByProperty(properties, comps, values);

        if (entities == null) {
            log.error("list() Error: Unable to list entities");
            return null;
        }

        log.info("list() returning list of entities");

        return entities;
    }

    /**
     * Updates the entity with the provided data.
     *
     * @return The updated entity or null if an error occurred.
     */
    public Entity update(Entity newEntity, Entity oldEntity) {

        log.info("update()");

        newEntity.setId(oldEntity.getId());
        newEntity.setLedgerEntryId(oldEntity.getLedgerEntryId());

        LedgerEntry ledgerEntry = newEntity.toLedgerEntry();
        boolean result = ledgerIntegrityService.createEntry(ledgerEntry);
        if (!result) {
            log.info("update() An error occurred: Unable to create a Ledger Entry");
            return null;
        }

        ObjectNode objectToCreate = this.objectMapper.valueToTree(newEntity);
        this.discoveryDatabase.deleteItem(oldEntity.getId());
        ObjectNode objectUpdated = this.discoveryDatabase.createItem(objectToCreate);

        if (objectUpdated == null) {
            log.error("update() Error: Unable to update Entity");
            return null;
        }

        Entity entityUpdated = null;
        try {
            entityUpdated = this.objectMapper.treeToValue(objectUpdated, Entity.class);
        } catch (JsonProcessingException e) {
            log.error("registerEntity() Error: ", e);
            return null;
        }

        log.info("update() returning the updated Entity");

        return entityUpdated;
    }
    
    /**
     * Deletes a given entry from the discovery.
     * @param id 
     */
    public void unregister(String id){
        log.info("unregister()");
        this.discoveryDatabase.deleteItem(id);
    }

    /**
     * Retrieve a node IP by providing the node ID
     * @param nodeId
     * @return the IP of the given node ID
     */
    public String getNodeUrl(String nodeId) {
        log.debug("getNodeIP() init");

        String nodeUrl = null;

        ObjectNode node = this.discoveryDatabase.getItem(nodeId);

        if (node == null) {
            log.debug("getNodeIP() node not found: " + nodeId);
            return nodeUrl;
        }

        try {
            Entity entity =  this.objectMapper.treeToValue(node, Entity.class);
            nodeUrl = entity.getProperties().get(Property.NAME_NODE_URL);
        } catch (JsonProcessingException e) {
            log.error("Error: ", e);
        }

        log.debug("getNodeIP() finished");

        return nodeUrl;
    }
}
