/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.services.node;

import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import pt.inov.eunomia.conf.NodeConfig;
import pt.inov.eunomia.model.discovery.Entity;
import pt.inov.eunomia.model.discovery.Property;
import pt.inov.eunomia.model.node.NodeInformation;
import pt.inov.eunomia.services.discovery.DiscoveryService;

/**
 * This implements all the logic related to this Node.
 * @author lmss
 */
@Service
public class NodeService {
    private static Logger logger=LoggerFactory.getLogger(NodeService.class);
    @Autowired
    private NodeConfig nodeConfig;
    @Autowired
    private DiscoveryService discoveryService;    
    private Entity discoveryEntity=null;

    private boolean readyFlag = false;
    
    /**
     * Initialize Node by doing a set of actions. 
     */
    public void initialize(){
        logger.info("initialize() Starting Node Service.");
        try {
            discoveryRegisterEntity();
            this.readyFlag = true;
        } catch (Exception e) {
            logger.error("initialize() Error: ", e);
        }
    }

    private synchronized void discoveryRegisterEntity() {
        logger.info("discoveryRegisterEntity()");
        NodeInformation nodeInformation=getNodeInformation();
        Entity entity=new Entity();
        entity.setId(nodeInformation.getId());
        entity.setType(Entity.NODE_TYPE);
        entity.getProperties().put(Property.NAME_NODE_DESCRIPTION, nodeInformation.getDescription());
        entity.getProperties().put(Property.NAME_NODE_PUB_KEY, nodeInformation.getPubKey());
        entity.getProperties().put(Property.NAME_VOTING_SERVER_ID, nodeInformation.getVotingServiceId());
        entity.getProperties().put(Property.NAME_NODE_URL, nodeInformation.getNodeUrl());
        discoveryEntity=discoveryService.registerEntity(entity);
        if (discoveryEntity == null) {
            logger.error("discoveryRegisterEntity() Error: Unable to create Entity");
        } else {
            logger.info("discoveryRegisterEntity() entity registered");
        }
    }
    
    @Scheduled(fixedRate = 21600000) //6 hours?!
    public void updateNodeInformation(){
        logger.info("updateNodeInformation() Called to update the discovery registry.");
        if (this.readyFlag) {
            discoveryRegisterEntity();
        }
    }
    
    
    public NodeInformation getNodeInformation(){

        logger.info("getNodeInformation()");

        NodeInformation nodeInformation = new NodeInformation();
        nodeInformation.setId(nodeConfig.getId());
        nodeInformation.setPubKey(nodeConfig.getPubKey());
        nodeInformation.setDescription(nodeConfig.getDescription());
        nodeInformation.setVotingServiceId(nodeConfig.getVotingServerId());
        nodeInformation.setNodeUrl(nodeConfig.getUrl());

        logger.info("getNodeInformation() returning Node Information");

        return nodeInformation;
    }
}
