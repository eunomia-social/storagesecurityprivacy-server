/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.services.storage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.inov.eunomia.conf.OrbitDbConfig;
import pt.inov.eunomia.model.ledger.LedgerEntry;
import pt.inov.eunomia.db.orbitdb.StorageOrbitDbClient;
import pt.inov.eunomia.model.storage.StorageObject;
import pt.inov.eunomia.util.RandomUtil;
import javax.annotation.PostConstruct;
import pt.inov.eunomia.services.ledger.LedgerIntegrityService;

@Service
public class StorageService {

    private static final Logger log = LoggerFactory.getLogger(StorageService.class);

    @Autowired
    private OrbitDbConfig orbitDbConfig;

    private StorageOrbitDbClient objectsDatabase;

    @Autowired
    private LedgerIntegrityService ledgerIntegrityService;

    private ObjectMapper objectMapper;

    @PostConstruct
    public void instantiateDatabases() {
        log.info("instantiateDatabases() Connection to Model database with the address");
        try {
            this.objectsDatabase = new StorageOrbitDbClient(
                    this.orbitDbConfig.getUrl(),
                    this.orbitDbConfig.getModelDatabaseCid(),
                    this.orbitDbConfig.getTimeoutMillis());
        } catch (Exception e) {
            log.error("instantiateDatabases() Error: ", e);
        }
        this.objectMapper = new ObjectMapper();
    }

    public StorageObject createObject(StorageObject storageObject) {

        log.info("createObject()");

        storageObject.setId(this.objectsDatabase.generateId());
        // TODO: Change Ledger Entry Id to a UUID
        storageObject.setLedgerEntryId(RandomUtil.generateRandomInt(1000000));

        LedgerEntry ledgerEntry = storageObject.toLedgerEntry();

        log.info("Creating the Ledger Entry: " + ledgerEntry);
        boolean result = ledgerIntegrityService.createEntry(ledgerEntry);
        if (!result) {
            log.error("createObject() Error: Unable to create a Ledger Entry");
            return null;
        }

        ObjectNode objectToCreate = this.objectMapper.valueToTree(storageObject);
        ObjectNode objectCreated = this.objectsDatabase.createItem(objectToCreate);

        if (objectCreated == null) {
            log.error("createObject() Error: Unable to create Storage Object");
            return null;
        }

        StorageObject storageObjectCreated = null;
        try {
            storageObjectCreated = this.objectMapper.treeToValue(objectCreated, StorageObject.class);
        } catch (JsonProcessingException e) {
            log.error("createObject() Error: ", e);
            return null;
        }
        
        storageObjectCreated.setValidated(true);

        log.info("createObject() returning Storage Object created");

        return storageObjectCreated;
    }

    public StorageObject getObject(String objectId) {

        log.info("getObject() with id: " + objectId);

        if (objectId.length() != StorageOrbitDbClient.ID_SIZE) return null;

        ObjectNode objectFetched = this.objectsDatabase.getItem(objectId);

        if (objectFetched == null) {
            log.error("getObject() Error: Unable to fetch Storage Object");
            return null;
        }

        StorageObject storageObject = null;
        try {
            storageObject = this.objectMapper.treeToValue(objectFetched, StorageObject.class);
        } catch (Exception e) {
            log.error("getObject() error: ", e);
        }

        log.info("getObject() returning the Storage Object fetched");

        return storageObject;
    }

    public StorageObject updateObject(StorageObject newStorageObject, StorageObject oldStorageObject) {
        log.info("updateObject() ");
        newStorageObject.setId(oldStorageObject.getId());
        newStorageObject.setLedgerEntryId(oldStorageObject.getLedgerEntryId());
        
        ObjectNode objectToCreate = this.objectMapper.valueToTree(newStorageObject);      
        boolean wasValid=ledgerIntegrityService.verifySignature(oldStorageObject);
        log.info("updateObject() Old Stored Object Signature is:"+wasValid);
       
        LedgerEntry ledgerEntry = newStorageObject.toLedgerEntry();
        boolean result = ledgerIntegrityService.createEntryDirectly(ledgerEntry);
        if (!result) {
            log.error("updateObject() error: Unable to create a Ledger Entry");
            return null;
        }
        
        this.objectsDatabase.deleteItem(oldStorageObject.getId());
        ObjectNode objectUpdated = this.objectsDatabase.createItem(objectToCreate);

        if (objectUpdated == null) {
            log.error("updateObject() Error: Unable to update Storage Object");
            return null;
        }
        
        newStorageObject.setValidated(wasValid);
        
        StorageObject storageObjectUpdated = null;
        try {
            storageObjectUpdated = this.objectMapper.treeToValue(objectUpdated, StorageObject.class);
        } catch (JsonProcessingException e) {
            log.error("updateObject() Error: ", e);
            return null;
        }

        log.info("updateObject() returning updated Storage Object");

        return newStorageObject;
    }

    public void deleteObject(String objectId) {

        log.info("deleteObject()");

        this.objectsDatabase.deleteItem(objectId);
        // TODO: set Ledger Entry as Deleted on the Ledger Service
    }

    public ArrayNode list(String[] properties, String[] comps, String[] values) {
        log.info("list()");
        ArrayNode storageObjects = this.objectsDatabase.getItemsByProperty(properties, comps, values);
        if (storageObjects == null) {
            log.error("list() Error: Unable to list storage objects");
            return null;
        }
        log.info("list() Returning "+storageObjects.size()+" documents.");
        return storageObjects;
    }
}
