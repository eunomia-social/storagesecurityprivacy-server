/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.services.track;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import pt.inov.eunomia.api.responses.ApiError;
import pt.inov.eunomia.conf.OrbitDbConfig;
import pt.inov.eunomia.model.ledger.LedgerEntry;
import pt.inov.eunomia.db.orbitdb.TrackOrbitDbClient;
import pt.inov.eunomia.model.storage.StorageObject;
import pt.inov.eunomia.model.tracking.TrackEntry;
import pt.inov.eunomia.util.RandomUtil;

import javax.annotation.PostConstruct;
import pt.inov.eunomia.services.ledger.LedgerIntegrityService;

@Service
public class TrackingService {

    private static final Logger log = LoggerFactory.getLogger(TrackingService.class);

    @Autowired
    private OrbitDbConfig orbitDbConfig;

    private TrackOrbitDbClient trackDatabase;

    @Autowired        
    private LedgerIntegrityService ledgerIntegrityService;
    
    private ObjectMapper objectMapper;

    @PostConstruct
    private void instantiateConnections() {
        log.info("instantiateConnections() Connection to Track database with the address");
        try {
            this.trackDatabase = new TrackOrbitDbClient(
                    this.orbitDbConfig.getUrl(),
                    this.orbitDbConfig.getTrackDatabaseCid(), this.orbitDbConfig.getTimeoutMillis());
        } catch (Exception e) {
            log.error("instantiateConnections() Error: ", e);
        }
        this.objectMapper = new ObjectMapper();
    }

    public TrackEntry createEntry(TrackEntry trackEntry) {
        log.info("createEntry()");
        trackEntry.setId(this.trackDatabase.generateId());
        // TODO: Change Ledger Entry Id to a UUID
        trackEntry.setLedgerEntryId(RandomUtil.generateRandomInt(1000000));
        LedgerEntry ledgerEntry = trackEntry.toLedgerEntry();
        log.info("Creating the Ledger Entry");
        boolean result = ledgerIntegrityService.createEntry(ledgerEntry);
        if (!result) {
            ApiError error = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Unable to create a Ledger Entry");
            log.info("createEntry() Error: " + error.getMessage());
            return null;
        }

        ObjectNode objectToCreate = this.objectMapper.valueToTree(trackEntry);
        ObjectNode objectCreated = this.trackDatabase.createItem(objectToCreate);

        if (objectCreated == null) {
            log.error("createEntry() Error: Unable to create Track Entry");
            return null;
        }

        TrackEntry trackEntryCreated = null;
        try {
            trackEntryCreated = this.objectMapper.treeToValue(objectCreated, TrackEntry.class);
        } catch (JsonProcessingException e) {
            log.error("createEntry() Error: ", e);
            return null;
        }
        trackEntryCreated.setValidated(true);
        log.info("createEntry() returning Track entry created");
        
        return trackEntryCreated;
    }

    public TrackEntry getEntry(String entryId) {

        log.info("getEntry()");

        ObjectNode objectFetched = this.trackDatabase.getItem(entryId);

        if (objectFetched == null) {
            log.error("getEntry() Error: Unable to fetch Track Entry");
            return null;
        }

        TrackEntry trackEntry = null;
        try {
            trackEntry = this.objectMapper.treeToValue(objectFetched, TrackEntry.class);
        } catch (Exception e) {
            log.error("getEntry() Error: ", e);
        }

        log.info("getEntry() returning the Track Entry fetched");

        return trackEntry;
    }

    public ArrayNode list(String[] properties, String[] comps, String[] values) {

        log.info("list()");

        ArrayNode trackEntries = this.trackDatabase.getItemsByProperty(properties, comps, values);

        if (trackEntries == null) {
            log.error("list() Error: Unable to fetch track entries");
            return null;
        }

        return trackEntries;
    }
}
