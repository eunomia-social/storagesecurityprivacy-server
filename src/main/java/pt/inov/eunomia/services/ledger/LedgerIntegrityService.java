package pt.inov.eunomia.services.ledger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.inov.eunomia.conf.LedgerConfig;
import pt.inov.eunomia.db.ledger.LedgerDbClient;
import pt.inov.eunomia.model.ledger.LedgerEntry;

import javax.annotation.PostConstruct;
import org.springframework.scheduling.annotation.Scheduled;
import pt.inov.eunomia.conf.OrbitDbConfig;
import pt.inov.eunomia.db.orbitdb.LedgerCacheOrbitDbClient;
import pt.inov.eunomia.model.LedgerEntryEntity;
import pt.inov.eunomia.model.ledger.LedgerEntriesIndexed;
import pt.inov.eunomia.model.ledger.LedgerEntryQuery;
import pt.inov.eunomia.util.Util;

/**
 * Service responsible for the integrity control using the External Ledger
 * Service.
 */
@Service
public class LedgerIntegrityService {
    private static final Logger log = LoggerFactory.getLogger(LedgerIntegrityService.class);

    @Autowired
    private LedgerConfig ledgerConfig;
    private LedgerDbClient ledgerDbClient;

    @Autowired
    private OrbitDbConfig orbitDbConfig;
    private LedgerCacheOrbitDbClient ledgerCacheOrbitDbClient;

    private ObjectMapper objectMapper;

    private boolean readyFlag = false;

    @PostConstruct
    private void instantiateConnections() {
        log.info("instantiateConnections() Connection to Ledger Service and database");
        try {
            this.ledgerDbClient = new LedgerDbClient(this.ledgerConfig.getUrl(), this.ledgerConfig.getTimeoutMillis());
            this.ledgerCacheOrbitDbClient = new LedgerCacheOrbitDbClient(orbitDbConfig.getUrl(), orbitDbConfig.getLedgerCacheDatabaseCid(), orbitDbConfig.getTimeoutMillis());
        } catch (Exception e) {
            log.error("instantiateConnections() Error: ", e);
        }
        this.objectMapper = new ObjectMapper();
    }

    public void initialize() {
        this.readyFlag = true;
        log.info("initialize()");
    }

    public void createEntry() {
    }

    // receive entry list and returns
    public void filter() {
    }

    /**
     * It creates a new ledger entry.
     *
     * @param ledgerEntry
     * @return True if everything ok, false otherwise.
     */
    public boolean createEntry(LedgerEntry ledgerEntry) {
        ObjectNode objectToCreate = this.objectMapper.valueToTree(ledgerEntry);
        if (ledgerConfig.isCacheEnabled()) {
            log.info("Creating Ledger entry in the cache database.");
            this.ledgerCacheOrbitDbClient.createItem(objectToCreate);
            return true;
        } else {
            return createEntryDirectly(ledgerEntry);
        }
    }

    /**
     * This methods creates a ledger entry directly in the blockchain.
     *
     * @param ledgerEntry
     * @return True if ok, false otherwise.
     */
    public boolean createEntryDirectly(LedgerEntry ledgerEntry) {
        log.info("Creating Ledger entry directly in ledger.");
        return this.ledgerDbClient.createEntry(ledgerEntry);
    }

    /**
     * This method is called each 5 seconds to submit new information to the
     * Ledger. Please note that currently we are using orbitDb as a distributed
     * cache, this can have the short coming that in same cases we will generate
     * duplicated information in the Ledger, since all the instances aren't
     * synchronized.
     */
    @Scheduled(fixedDelayString = "${ledger.cache_polling_period_millis}")
    public void submitPendingItemsToLedger() {
        if (!ledgerConfig.isCacheEnabled() || !readyFlag) {
            return;
        }
        //get pending ledger entries
        ArrayNode arrayNode = this.ledgerCacheOrbitDbClient.getItemsByProperty(null, null, null);
        if (arrayNode == null || arrayNode.size() == 0) {
            log.debug("submitPendingItemsToLedger() Nothing to submit.");
            return;
        }
        log.info("submitPendingItemsToLedger() Found " + arrayNode.size() + " cached entries. Submiting them to the ledger.");
        for (int n = 0; n < arrayNode.size(); n++) {
            JsonNode objectFetched = arrayNode.get(n);
            try {
                LedgerEntry ledgerEntry = (LedgerEntry) this.objectMapper.treeToValue(objectFetched, LedgerEntry.class);
                //check if it was already submited
                LedgerEntry existingLedgerEntry = get(ledgerEntry.getId(), ledgerEntry.getType());
                if (existingLedgerEntry == null) {
                    log.debug("submitPendingItemsToLedger() Submiting new item from distributed cache to the Ledger:" + ledgerEntry);
                    this.ledgerDbClient.createEntry(ledgerEntry);
                    this.ledgerCacheOrbitDbClient.deleteItem("" + ledgerEntry.getId());
                } else {
                    //if it's already there... delete it anyway.
                    this.ledgerCacheOrbitDbClient.deleteItem("" + ledgerEntry.getId());
                }
            } catch (Exception e) {
                log.error("submitPendingItemsToLedger() Error processing item:" + n + " from " + arrayNode, e);
            }
        }
    }

    /**
     * Verifies the signature for a given object against the ledger.
     *
     * @param obj The object that implements a LedgerEntryEntity to be validated against the ledger.
     *
     * @return False if an error occur or the signature isn't the same in the
     * ledger. The corresponding validated flag is set accordingly.
     */
    public boolean verifySignature(LedgerEntryEntity obj) {
        if (obj == null) {
            log.warn("verifySignature() Returning false due to null ledgerEntryEntity received.");
            return false;
        }
        LedgerEntry objectLedgerEntry = obj.toLedgerEntry();
        String type = objectLedgerEntry.getType();
        int ledgerEntryId = objectLedgerEntry.getId();
        log.info("verifySignature() type:" + type + " ledgerEntryId:" + ledgerEntryId);
        if (ledgerConfig.isDisableSignature()) { //it can be disabled from the configuration.
            log.info("verifySignature() type:" + type + " ledgerEntryId:" + ledgerEntryId + " signature considered ok since disableSignature is enabled.");
            obj.setValidated(true);
            return true;
        }
        String storageObjectHash = null;
        try {
            storageObjectHash = Util.bytesToHex(Util.toHash(obj));
        } catch (Exception e) {
            log.error("verifySignature() Error hashing object.", e);
            return false;
        }
        LedgerEntry ledgerEntry = get(ledgerEntryId, type);
        if (ledgerEntry == null) {
            log.warn("verifySignature() Couldn't get the ledgerEntryId:" + ledgerEntryId + " from blockchain, create it imediatelly!.");

            if (createEntryDirectly(objectLedgerEntry)) {
                if (this.ledgerCacheOrbitDbClient.getItem("" + objectLedgerEntry.getId()) != null) {
                    this.ledgerCacheOrbitDbClient.deleteItem("" + objectLedgerEntry.getId());
                }
                obj.setValidated(true);
                return true;
            } else {
                return false;
            }
        }

        boolean signatureValid = storageObjectHash.equals(ledgerEntry.getSignature());
        log.info("verifySignature() type:" + type + " ledgerEntryId:" + ledgerEntryId + " signature ok?" + signatureValid);
        if (signatureValid) {
            obj.setValidated(true);
        }
        return signatureValid;
    }

    /**
     * It validates a batch of objects against the ledger.
     *
     * @param objs A list of object that implement a LedgerEntryEntity to be validated against the ledger.
     * 
     * @return True if everything went ok and all of them are valid, false
     * otherwise. The corresponding validated flag is set accordingly.
     */
    public boolean verifySignatures(ArrayList<LedgerEntryEntity> objs) {
        boolean ret = false;
        if (objs == null) {
            log.warn("verifySignatures() Returning true a null list of ledger entry entities received.");
            return true;
        }
        if (ledgerConfig.isDisableSignature()) { //it can be disabled from the configuration.
            log.info("verifySignatures() Signatures considered ok since disableSignature is enabled.");
            for (LedgerEntryEntity ledgerEntryEntity : objs) {
                ledgerEntryEntity.setValidated(true);
            }
            return true;
        }
        if(objs.isEmpty()){
            log.warn("verifySignatures() Returning true due to an empty list of ledger entry entities received.");
            return true;
        }
        ArrayList<LedgerEntryQuery> ledgerQuery = new ArrayList<>();
        for (LedgerEntryEntity ledgerEntityEntry : objs) {
            LedgerEntry ledgerEntry = ledgerEntityEntry.toLedgerEntry();
            ledgerQuery.add(new LedgerEntryQuery(ledgerEntry));
        }
        log.info("verifySignatures() Validating signatures for query:"+ledgerQuery);
        LedgerEntriesIndexed ledgerEntriesIndexed = ledgerDbClient.getEntries(ledgerQuery);
        if (ledgerEntriesIndexed == null) {
            log.warn("verifySignatures() Returning false because couldn't get a list of ledger entries.");
            return false;
        }

        for (LedgerEntryEntity obj : objs) {
            obj.setValidated(false);
            LedgerEntry ledgerEntry = obj.toLedgerEntry();
            LedgerEntry existingLedgerEntry = ledgerEntriesIndexed.getByTypeById(ledgerEntry.getType(), "" + ledgerEntry.getId());
            if (existingLedgerEntry != null) {
                try {
                    String storageObjectHash = Util.bytesToHex(Util.toHash(obj));
                    boolean signatureValid = storageObjectHash.equals(ledgerEntry.getSignature());
                    if(signatureValid){
                        obj.setValidated(true);
                    }else{
                        obj.setValidated(false);
                    }
                } catch (Exception e) {
                    log.error("verifySignatures() Error hashing object.", e);
                    ret = false;
                    break;
                }
            } else {
                //todo should we try to create new entries? They can be missing...
                ret = false;
            }
        }
        return ret;
    }

    /**
     * Get a given Ledger Entry by id and type.
     *
     * @param ledgerEntryId The ledger entry id.
     * @param type The ledger type.
     * @return A LedgerEntry if it exists or null otherwise.
     */
    public LedgerEntry get(int ledgerEntryId, String type) {
        return this.ledgerDbClient.getEntry(ledgerEntryId, type);
    }
}
