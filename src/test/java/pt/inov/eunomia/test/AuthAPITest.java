/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package pt.inov.eunomia.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import pt.inov.eunomia.api.filters.AuthFilter;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"scheduler=false"})
@AutoConfigureMockMvc
public class AuthAPITest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private AuthFilter authFilter; 
    
    @Before
    public void setup() {
        //simulate an init - unfortunatelly WebMVCMock doesn't call filter.init :(
        try{
            authFilter.init(null);
        }catch(Exception e){
           e.printStackTrace();
        }
    }
    
    @Test
    @Order(1)
    public void getEunomiaToken() throws Exception{
        String accessToken="Yr0s7gYKLQvxof17VBsBlrNMNC_se8d4AOz4APCfKNI";
        String authProviderURI="mastodon.social";
        mvc.perform(get("/eunomia/token").servletPath("/eunomia/token")
                .param("access_token",accessToken)
                .param("auth_provider_uri", authProviderURI)
        ).andExpect(status().isOk());
    }
    
}
