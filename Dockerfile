FROM openjdk:8-jdk-alpine

# RUN addgroup -S spring && adduser -S spring -G spring
# USER spring:spring

ARG JAR_FILE=target/p2p-server-standalone*.jar

VOLUME [ "/storage-server" ]

ADD ${JAR_FILE} storage-server/server.jar

ENTRYPOINT ["java","-jar","storage-server/server.jar"]